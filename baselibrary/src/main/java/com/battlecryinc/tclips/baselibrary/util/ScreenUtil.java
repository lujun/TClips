package com.battlecryinc.tclips.baselibrary.util;

import android.content.res.Resources;
import android.util.DisplayMetrics;

/**
 * Created by geoff on 17/3/28.
 */

public class ScreenUtil {

    private DisplayMetrics displayMetrics;

    private ScreenUtil(){
        displayMetrics = Resources.getSystem().getDisplayMetrics();
    }

    private static class Holder{

        private static ScreenUtil holder = new ScreenUtil();
    }

    public int getScreenWidth() {
        return displayMetrics.widthPixels;
    }

    public int getScreenHeight() {
        return displayMetrics.heightPixels;
    }

    public static ScreenUtil get(){
        return Holder.holder;
    }
}
