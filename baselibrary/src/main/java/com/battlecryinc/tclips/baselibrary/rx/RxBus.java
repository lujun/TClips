package com.battlecryinc.tclips.baselibrary.rx;

import io.reactivex.Flowable;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;

/**
 * Created by geoff on 17/3/24.
 */

public class RxBus {

    private RxBus(){}

    private static class Holder {

        private static RxBus rxBus = new RxBus();
    }

    public static RxBus get(){
        return Holder.rxBus;
    }

    private final FlowableProcessor<Object> _bus = PublishProcessor.create().toSerialized();

    public void post(Object o) {
        _bus.onNext(o);
    }

    public <T> Flowable<T> toFlowable(Class<T> eventType) {
        return _bus.ofType(eventType);
    }

    public boolean hasSubscribers(){
        return _bus.hasSubscribers();
    }
}
