package com.battlecryinc.tclips.router.db;

import com.battlecryinc.tclips.baselibrary.app.App;
import com.battlecryinc.tclips.router.BuildConfig;
import com.battlecryinc.tclips.router.db.table.DaoMaster;
import com.battlecryinc.tclips.router.db.table.DaoSession;

import org.greenrobot.greendao.database.Database;

/**
 * Created by geoff on 17/3/30.
 */

public class DB {

    private DaoSession mDaoSession;

    private DB(){
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(App.INSTANCE,
                BuildConfig.DEBUG ? BuildConfig.DB_NAME : BuildConfig.DB_NAME_ENCRYPTED);
//        Database clipsDB = BuildConfig.DEBUG ? devOpenHelper.getWritableDb() :
//                devOpenHelper.getEncryptedWritableDb(BuildConfig.DB_NAME_ENCRYPTED_PWD);
        Database clipsDB = devOpenHelper.getWritableDb();
        mDaoSession = new DaoMaster(clipsDB).newSession();
    }

    private static class Holder{

        private static DB db = new DB();
    }

    public static DB get(){
        return Holder.db;
    }

    public DaoSession getDaoSession(){
        return mDaoSession;
    }
}
