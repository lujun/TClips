package com.battlecryinc.tclips.router.retrofit;

import com.battlecryinc.tclips.router.BuildConfig;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by geoff on 17/3/31.
 */

public class HttpServiceUtil {

    public static <T> T getCustomClientService(Class<T> clazz, OkHttpClient client){
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.TWITCH_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
                .create(clazz);
    }
}
