package com.battlecryinc.tclips.router.db.table;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by geoff on 17/3/30.
 */

@Entity(indexes = {
    @Index(value = "name", unique = true)
})
public class User {

    @Id
    private Long _id;

    @NotNull
    private String name;

    private String display_name;

    private String bio;

    private String logo;

    private String email;

    @NotNull
    private String twitchToken;

    private String serverToken;

    @NotNull
    private Boolean currentLogin;

    private String followedChannel;

    @Generated(hash = 205885968)
    public User(Long _id, @NotNull String name, String display_name, String bio,
            String logo, String email, @NotNull String twitchToken,
            String serverToken, @NotNull Boolean currentLogin,
            String followedChannel) {
        this._id = _id;
        this.name = name;
        this.display_name = display_name;
        this.bio = bio;
        this.logo = logo;
        this.email = email;
        this.twitchToken = twitchToken;
        this.serverToken = serverToken;
        this.currentLogin = currentLogin;
        this.followedChannel = followedChannel;
    }

    @Generated(hash = 586692638)
    public User() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTwitchToken() {
        return twitchToken;
    }

    public void setTwitchToken(String twitchToken) {
        this.twitchToken = twitchToken;
    }

    public String getServerToken() {
        return serverToken;
    }

    public void setServerToken(String serverToken) {
        this.serverToken = serverToken;
    }

    public Boolean getCurrentLogin() {
        return currentLogin;
    }

    public void setCurrentLogin(Boolean currentLogin) {
        this.currentLogin = currentLogin;
    }

    public String getFollowedChannel() {
        return followedChannel;
    }

    public void setFollowedChannel(String followedChannel) {
        this.followedChannel = followedChannel;
    }
}
