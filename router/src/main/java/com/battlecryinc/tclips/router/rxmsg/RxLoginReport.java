package com.battlecryinc.tclips.router.rxmsg;

/**
 * Created by geoff on 17/3/31.
 */

public class RxLoginReport {

    private boolean login;

    public boolean isLogin() {
        return login;
    }

    public RxLoginReport(boolean login){
        this.login = login;
    }
}
