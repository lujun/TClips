package com.battlecryinc.tclips.router.activityhelper;

/**
 * Created by geoff on 17/3/23.
 */

public class ActivityList {

    public static final String ATY_AUTH = "com.battlecryinc.tclips.login.view.AuthActivity";
    public static final String ATY_CLIPS = "com.battlecryinc.tclips.clips.view.clips.ClipsActivity";
}
