package com.battlecryinc.tclips.router.activityhelper;

import android.content.Context;
import android.content.Intent;

/**
 * Created by geoff on 17/3/23.
 */

public class ActivityLaunchHelper {

    public static void startActivityForName(Context context, String name) {
        try {
            Class clazz = Class.forName(name);
            context.startActivity(new Intent(context, clazz));
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
