package com.battlecryinc.mvptest.module;

import com.battlecryinc.mvptest.view.IView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by geoff on 17/4/17.
 */


@Module
public class TestModule {

    private IView view;

    public TestModule(IView view){
        this.view = view;
    }

    @Provides
    IView provideIView(){
        return view;
    }

}
