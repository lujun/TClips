package com.battlecryinc.mvptest.view;

/**
 * Created by geoff on 17/4/17.
 */

public interface IView {

    void hideProgressbar(boolean hide);
    void clearTextView();
}
