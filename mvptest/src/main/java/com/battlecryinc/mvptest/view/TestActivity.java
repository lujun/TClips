package com.battlecryinc.mvptest.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.battlecryinc.mvptest.R;
import com.battlecryinc.mvptest.component.DaggerTestComponent;
import com.battlecryinc.mvptest.module.TestModule;
import com.battlecryinc.mvptest.presenter.TestPresenter;

import javax.inject.Inject;

/**
 * Created by geoff on 17/4/17.
 */

public class TestActivity extends AppCompatActivity implements IView, View.OnClickListener {

    TextView textView;
    ProgressBar progressBar;
    Button button;

    @Inject
    TestPresenter testPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        textView = (TextView) findViewById(R.id.textView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        button = (Button) findViewById(R.id.button);

        button.setOnClickListener(this);

        DaggerTestComponent.builder().testModule(new TestModule(this)).build().inject(this);
    }

    @Override
    public void hideProgressbar(boolean hide) {
        progressBar.setVisibility(hide ? View.GONE : View.VISIBLE);
    }

    @Override
    public void clearTextView() {
        textView.setText("");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button){
            testPresenter.clear();
        }
    }
}
