package com.battlecryinc.mvptest.component;

import com.battlecryinc.mvptest.module.TestModule;
import com.battlecryinc.mvptest.view.TestActivity;

import dagger.Component;

/**
 * Created by geoff on 17/4/17.
 */

@Component(modules = TestModule.class)
public interface TestComponent {

    void inject(TestActivity activity);
}
