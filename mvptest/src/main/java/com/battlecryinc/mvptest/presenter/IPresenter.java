package com.battlecryinc.mvptest.presenter;

/**
 * Created by geoff on 17/4/17.
 */

public interface IPresenter {

    void clear();
    void doSomething();
}
