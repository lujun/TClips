package com.battlecryinc.mvptest.presenter;

import android.os.Handler;
import android.os.Looper;

import com.battlecryinc.mvptest.view.IView;

import javax.inject.Inject;

/**
 * Created by geoff on 17/4/17.
 */

public class TestPresenter implements IPresenter {

    IView iView;

    static final String TAG = "TestPresenter";

    @Inject
    public TestPresenter(IView view){
        iView = view;
    }

    @Override
    public void clear() {
        doSomething();
    }

    @Override
    public void doSomething() {
        iView.hideProgressbar(false);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                iView.hideProgressbar(true);
                iView.clearTextView();
            }
        }, 3000);
    }
}
