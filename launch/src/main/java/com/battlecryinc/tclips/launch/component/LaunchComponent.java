package com.battlecryinc.tclips.launch.component;

import com.battlecryinc.tclips.launch.module.LaunchModule;
import com.battlecryinc.tclips.launch.view.LaunchActivity;

import dagger.Component;

/**
 * Created by geoff on 17/4/17.
 */

@Component(modules = LaunchModule.class)
public interface LaunchComponent {

    void inject(LaunchActivity activity);
}
