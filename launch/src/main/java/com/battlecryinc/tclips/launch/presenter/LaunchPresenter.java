package com.battlecryinc.tclips.launch.presenter;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;

import com.battlecryinc.tclips.launch.contract.LaunchContract;
import com.battlecryinc.tclips.router.BuildConfig;
import com.battlecryinc.tclips.router.activityhelper.ActivityLaunchHelper;
import com.battlecryinc.tclips.router.activityhelper.ActivityList;
import com.battlecryinc.tclips.router.db.DB;
import com.battlecryinc.tclips.router.db.table.User;
import com.battlecryinc.tclips.router.db.table.UserDao;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tencent.bugly.crashreport.CrashReport;

import org.greenrobot.greendao.query.QueryBuilder;

import javax.inject.Inject;

/**
 * Created by geoff on 17/3/30.
 */

public class LaunchPresenter implements LaunchContract.ILaunchPresenter {

    private LaunchContract.ILaunchView iView;

    private static final int DELAY_LAUNCH_TIME = 1500;

    @Inject
    public LaunchPresenter(LaunchContract.ILaunchView view){
        iView = view;
    }

    @Override
    public void start() {
        final Activity activity = iView.getActivity();
        final Context appContext = activity.getApplicationContext();
        // init fresco
        Fresco.initialize(appContext);
        // init firebase
        FirebaseAnalytics.getInstance(appContext);
        // init bugly
        CrashReport.initCrashReport(appContext, BuildConfig.BUGLY_APP_ID, BuildConfig.DEBUG);
        // init account
        initAccountData();
        // goto clips
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ActivityLaunchHelper.startActivityForName(activity, ActivityList.ATY_CLIPS);
                activity.finish();
            }
        }, DELAY_LAUNCH_TIME);
    }

    @Override
    public void initAccountData(){
        QueryBuilder<User> userQuery = DB.get().getDaoSession().getUserDao().queryBuilder()
                .where(UserDao.Properties.CurrentLogin.eq(true));
        // user login
        if (userQuery.count() > 0){
//            userQuery.list().get(0)
        }
    }
}
