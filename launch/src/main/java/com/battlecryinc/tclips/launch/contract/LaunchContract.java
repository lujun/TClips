package com.battlecryinc.tclips.launch.contract;

import android.app.Activity;

import com.battlecryinc.tclips.baselibrary.mvp.BasePresenter;
import com.battlecryinc.tclips.baselibrary.mvp.BaseView;

/**
 * Created by geoff on 17/4/18.
 */

public interface LaunchContract {

    interface ILaunchView extends BaseView<ILaunchPresenter> {

        Activity getActivity();
    }

    interface ILaunchPresenter extends BasePresenter {

        void initAccountData();
    }
}
