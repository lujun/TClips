package com.battlecryinc.tclips.launch.view;

import android.app.Activity;
import android.os.Bundle;

import com.battlecryinc.tclips.launch.R;
import com.battlecryinc.tclips.launch.component.DaggerLaunchComponent;
import com.battlecryinc.tclips.launch.contract.LaunchContract;
import com.battlecryinc.tclips.launch.module.LaunchModule;
import com.battlecryinc.tclips.launch.presenter.LaunchPresenter;
import com.battlecryinc.tclips.router.baseview.BaseActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class LaunchActivity extends BaseActivity implements LaunchContract.ILaunchView {

    @Inject
    LaunchPresenter launchPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        ButterKnife.bind(this);

        DaggerLaunchComponent.builder().launchModule(new LaunchModule(this)).build().inject(this);

        launchPresenter.start();
    }

    @Override
    public Activity getActivity() {
        return this;
    }
}
