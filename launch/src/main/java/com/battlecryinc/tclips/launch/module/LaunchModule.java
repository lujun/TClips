package com.battlecryinc.tclips.launch.module;

import com.battlecryinc.tclips.launch.contract.LaunchContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by geoff on 17/4/17.
 */

@Module
public class LaunchModule {

    private final LaunchContract.ILaunchView view;

    public LaunchModule(LaunchContract.ILaunchView view){
        this.view = view;
    }

    @Provides
    LaunchContract.ILaunchView provideILaunchView(){
        return view;
    }
}
