package com.battlecryinc.tclips.login.retrofit.service;

import com.battlecryinc.tclips.login.bean.TwitchToken;
import com.battlecryinc.tclips.login.bean.TwitchUser;
import com.battlecryinc.tclips.router.BuildConfig;

import io.reactivex.Flowable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by geoff on 17/3/24.
 */

public interface TwitchService {

    @FormUrlEncoded
    @POST("/kraken/oauth2/token")
    Flowable<TwitchToken> getTwitchToken(
            @Field("client_id") String client_id, @Field("client_secret") String client_secret,
            @Field("grant_type") String grant_type, @Field("redirect_uri") String redirect_uri,
            @Field("code") String code, @Field("state") String state);

    @Headers({
            "Accept: application/vnd.twitchtv.v5+json",
            "Client-ID: " + BuildConfig.TWITCH_CLIENT_ID
    })
    @GET("/kraken/user")
    Flowable<TwitchUser> getTwitchUser();
}
