package com.battlecryinc.tclips.login.bean;

import java.util.List;

/**
 * Author: lujun(http://blog.lujun.co)
 * Date: 29/03/2017 22:54
 */

public class TwitchToken {

    /**
     * access_token : urjtrtnh3yl4qund7e1ki8re404r07
     * refresh_token : xyb1yo22yxd1cb65m1np1igyz34psafd1365f43y1b5xlouwfg
     * scope : ["user_blocks_read","user_follows_edit","user_read","user_subscriptions"]
     */

    private String access_token;
    private String refresh_token;
    private List<String> scope;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public List<String> getScope() {
        return scope;
    }

    public void setScope(List<String> scope) {
        this.scope = scope;
    }
}
