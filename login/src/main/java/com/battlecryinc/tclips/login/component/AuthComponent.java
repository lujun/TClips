package com.battlecryinc.tclips.login.component;

import com.battlecryinc.tclips.login.module.AuthModule;
import com.battlecryinc.tclips.login.view.AuthActivity;

import dagger.Component;

/**
 * Created by geoff on 17/4/17.
 */

@Component(modules = AuthModule.class)
public interface AuthComponent {

    void inject(AuthActivity authActivity);
}
