package com.battlecryinc.tclips.login.bean;

/**
 * Author: lujun(http://blog.lujun.co)
 * Date: 29/03/2017 23:11
 */

public class TwitchUser {

    /**
     * display_name : whilu
     * _id : 147681478
     * name : whilu
     * type : user
     * bio : null
     * created_at : 2017-02-13T08:26:58.800686Z
     * updated_at : 2017-03-29T15:37:00.739474Z
     * logo : null
     * email : lujun.byte@gmail.com
     * email_verified : true
     * partnered : false
     * twitter_connected : false
     * notifications : {"push":true,"email":true}
     */

    private String display_name;
    private String _id;
    private String name;
    private String type;
    private String bio;
    private String created_at;
    private String updated_at;
    private String logo;
    private String email;
    private boolean email_verified;
    private boolean partnered;
    private boolean twitter_connected;
    private NotificationsBean notifications;

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmail_verified() {
        return email_verified;
    }

    public void setEmail_verified(boolean email_verified) {
        this.email_verified = email_verified;
    }

    public boolean isPartnered() {
        return partnered;
    }

    public void setPartnered(boolean partnered) {
        this.partnered = partnered;
    }

    public boolean isTwitter_connected() {
        return twitter_connected;
    }

    public void setTwitter_connected(boolean twitter_connected) {
        this.twitter_connected = twitter_connected;
    }

    public NotificationsBean getNotifications() {
        return notifications;
    }

    public void setNotifications(NotificationsBean notifications) {
        this.notifications = notifications;
    }

    public static class NotificationsBean {
        /**
         * push : true
         * email : true
         */

        private boolean push;
        private boolean email;

        public boolean isPush() {
            return push;
        }

        public void setPush(boolean push) {
            this.push = push;
        }

        public boolean isEmail() {
            return email;
        }

        public void setEmail(boolean email) {
            this.email = email;
        }
    }
}
