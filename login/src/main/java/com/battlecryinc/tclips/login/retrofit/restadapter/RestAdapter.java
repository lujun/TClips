package com.battlecryinc.tclips.login.retrofit.restadapter;

import com.battlecryinc.tclips.login.retrofit.service.TwitchService;
import com.battlecryinc.tclips.router.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by geoff on 17/3/24.
 */

public class RestAdapter {

    private TwitchService twitchService;

    public TwitchService getTwitchService() {
        return twitchService;
    }

    private RestAdapter(){
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY :
                HttpLoggingInterceptor.Level.NONE);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .build();

        twitchService = new Retrofit.Builder()
                .baseUrl(BuildConfig.TWITCH_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
                .create(TwitchService.class);
    }

    private static class Holder{

        private static RestAdapter twitchRestAdapter = new RestAdapter();
    }

    public static RestAdapter get(){
        return Holder.twitchRestAdapter;
    }
}
