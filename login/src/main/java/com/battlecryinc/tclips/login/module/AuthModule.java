package com.battlecryinc.tclips.login.module;

import com.battlecryinc.tclips.login.contract.AuthContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by geoff on 17/4/17.
 */

@Module
public class AuthModule {

    private final AuthContract.IAuthView view;

    public AuthModule(AuthContract.IAuthView view){
        this.view = view;
    }

    @Provides
    AuthContract.IAuthView provideIAuthView(){
        return view;
    }
}
