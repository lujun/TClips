package com.battlecryinc.tclips.login.contract;

import android.app.Activity;

import com.battlecryinc.tclips.baselibrary.mvp.BasePresenter;
import com.battlecryinc.tclips.baselibrary.mvp.BaseView;
import com.battlecryinc.tclips.login.bean.TwitchUser;

/**
 * Created by geoff on 17/4/18.
 */

public interface AuthContract {

    interface IAuthView extends BaseView<IAuthPresenter> {

        Activity getActivity();
        void setProgressBarVisible(boolean visible);
        void initWebView();
        void loadUrl(String url);
    }

    interface IAuthPresenter extends BasePresenter {

        void onLoadAuthPage();
        void onAuthFailed();
        void getTwitchToken(String code);
        void getUserData(final String token);
        void saveOrUpdateUser(TwitchUser tUser, String token);
    }
}
