package com.battlecryinc.tclips.login.presenter;

import android.widget.Toast;

import com.battlecryinc.tclips.baselibrary.rx.RxBus;
import com.battlecryinc.tclips.login.R;
import com.battlecryinc.tclips.login.bean.TwitchToken;
import com.battlecryinc.tclips.login.bean.TwitchUser;
import com.battlecryinc.tclips.login.contract.AuthContract;
import com.battlecryinc.tclips.login.retrofit.restadapter.RestAdapter;
import com.battlecryinc.tclips.login.retrofit.service.TwitchService;
import com.battlecryinc.tclips.router.BuildConfig;
import com.battlecryinc.tclips.router.db.DB;
import com.battlecryinc.tclips.router.db.table.User;
import com.battlecryinc.tclips.router.db.table.UserDao;
import com.battlecryinc.tclips.router.retrofit.HttpServiceUtil;
import com.battlecryinc.tclips.router.rxmsg.RxLoginReport;

import org.greenrobot.greendao.query.QueryBuilder;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by geoff on 17/3/29.
 */

public class AuthPresenter implements AuthContract.IAuthPresenter {

    private String authUrl;
    private AuthContract.IAuthView iAuthView;

    @Inject
    public AuthPresenter(AuthContract.IAuthView view){
        iAuthView = view;
    }

    @Override
    public void start() {
        authUrl = BuildConfig.TWITCH_HOST + "/kraken/oauth2/authorize" +
                "?response_type=code" + "&client_id=" + BuildConfig.TWITCH_CLIENT_ID +
                "&redirect_uri=" + BuildConfig.TWITCH_AUTH_REDIRECT_URL +
                "&scope=" + BuildConfig.TWITCH_AUTH_SCOPE +
                "&state=" + System.currentTimeMillis() +
                "&force_verify=true";
        onLoadAuthPage();
    }

    @Override
    public void onAuthFailed(){
        Toast.makeText(iAuthView.getActivity(), R.string.login_auth_failed, Toast.LENGTH_SHORT).show();
        onLoadAuthPage();
    }

    @Override
    public void onLoadAuthPage(){
        iAuthView.loadUrl(authUrl);
    }

    @Override
    public void getTwitchToken(String code){
        iAuthView.setProgressBarVisible(true);
        RestAdapter.get().getTwitchService().getTwitchToken(BuildConfig.TWITCH_CLIENT_ID,
                BuildConfig.TWITCH_CLIENT_SECRET, "authorization_code",
                BuildConfig.TWITCH_AUTH_REDIRECT_URL, code,
                String.valueOf(System.currentTimeMillis()))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<TwitchToken>() {
                    @Override
                    public void accept(@NonNull TwitchToken twitchToken) throws Exception {
                        if (twitchToken == null){
                            iAuthView.setProgressBarVisible(false);
                            onAuthFailed();
                        }else {
                            getUserData(twitchToken.getAccess_token());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        iAuthView.setProgressBarVisible(false);
                        onAuthFailed();
                    }
                });
    }

    @Override
    public void getUserData(final String token){
        Interceptor authInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originReq = chain.request();
                Request curReq = originReq.newBuilder()
                        .header("Authorization", "OAuth " + token)
                        .method(originReq.method(), originReq.body())
                        .build();
                return chain.proceed(curReq);
            }
        };
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY :
                HttpLoggingInterceptor.Level.NONE);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(authInterceptor)
                .build();

        HttpServiceUtil.getCustomClientService(TwitchService.class, client).getTwitchUser()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<TwitchUser>() {
                    @Override
                    public void accept(@NonNull TwitchUser twitchUser) throws Exception {
                        iAuthView.setProgressBarVisible(false);
                        if (twitchUser == null) {
                            onAuthFailed();
                        }else {
                            saveOrUpdateUser(twitchUser, token);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        iAuthView.setProgressBarVisible(false);
                        onAuthFailed();
                    }
                });
    }

    @Override
    public void saveOrUpdateUser(TwitchUser tUser, String token){
        UserDao userDao = DB.get().getDaoSession().getUserDao();
        List<User> users = userDao.queryBuilder().list();
        for (User user : users) {
            if (user.getCurrentLogin()) user.setCurrentLogin(false);
        }
        userDao.updateInTx(users);

        Long cUid = Long.valueOf(tUser.get_id());
        String cUserFChannels = null;
        QueryBuilder<User> userQuery = userDao.queryBuilder().where(UserDao.Properties._id.eq(cUid));
        if (userQuery.count() > 0){
            cUserFChannels = userQuery.list().get(0).getFollowedChannel();
        }

        User user = new User(cUid, tUser.getName(), tUser.getDisplay_name(),
                tUser.getBio(), tUser.getLogo(), tUser.getEmail(), token, null, true, cUserFChannels);
        if (userDao.insertOrReplace(user) > 0){
            RxBus.get().post(new RxLoginReport(true));
            iAuthView.getActivity().finish();
        }else {
            onAuthFailed();
        }
    }
}
