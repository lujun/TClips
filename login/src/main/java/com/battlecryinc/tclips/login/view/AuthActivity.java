package com.battlecryinc.tclips.login.view;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.battlecryinc.tclips.baselibrary.util.ScreenUtil;
import com.battlecryinc.tclips.login.R;
import com.battlecryinc.tclips.login.R2;
import com.battlecryinc.tclips.login.component.DaggerAuthComponent;
import com.battlecryinc.tclips.login.contract.AuthContract;
import com.battlecryinc.tclips.login.module.AuthModule;
import com.battlecryinc.tclips.login.presenter.AuthPresenter;
import com.battlecryinc.tclips.router.BuildConfig;
import com.battlecryinc.tclips.router.baseview.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by geoff on 17/3/29.
 */

public class AuthActivity extends BaseActivity implements AuthContract.IAuthView {

    @BindView(R2.id.wv_auth)
    WebView wvAuth;
    @BindView(R2.id.login_progressbar)
    ProgressBar progressBar;
    @BindView(R2.id.login_wv_progress)
    View viewProgress;

    @Inject
    AuthPresenter authPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity_auth);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.login_twitch_login);
        }

        ButterKnife.bind(this);

        initWebView();

        DaggerAuthComponent.builder().authModule(new AuthModule(this)).build().inject(this);

        authPresenter.start();
    }

    @Override
    public void initWebView(){
        wvAuth.getSettings().setJavaScriptEnabled(true);
        wvAuth.setWebViewClient(new WebViewClient(){

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                if (viewProgress.isShown()) viewProgress.setVisibility(View.GONE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (!viewProgress.isShown()) viewProgress.setVisibility(View.VISIBLE);
                if (url.startsWith(BuildConfig.TWITCH_AUTH_REDIRECT_URL)) {
                    Uri uri = Uri.parse(url);
                    authPresenter.getTwitchToken(uri.getQueryParameter("code"));
                    return true;
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (!viewProgress.isShown()) viewProgress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (viewProgress.isShown()) viewProgress.setVisibility(View.GONE);
            }
        });
        wvAuth.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                ViewGroup.LayoutParams params = viewProgress.getLayoutParams();
                params.width = (int) ((float) newProgress / 100 * ScreenUtil.get().getScreenWidth());
                viewProgress.setLayoutParams(params);
            }
        });
    }

    @Override
    public void loadUrl(String url){
        wvAuth.loadUrl(url);
    }

    @Override
    public void setProgressBarVisible(boolean visible){
        if (progressBar == null){
            return;
        }
        progressBar.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (wvAuth.canGoBack()){
            wvAuth.goBack();
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Activity getActivity() {
        return this;
    }
}
