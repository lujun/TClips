package com.battlecryinc.tclips.clips.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by geoff on 17/3/27.
 */

public class Clip {

    /**
     * clips : [{"id":"PoliteApatheticAlligatorMingLee","tracking_id":"61818131","url":"https://clips.twitch.tv/PoliteApatheticAlligatorMingLee?tt_medium=clips_api&tt_content=url","embed_url":"https://clips.twitch.tv/embed?clip=PoliteApatheticAlligatorMingLee&tt_medium=clips_api&tt_content=embed","embed_html":"<iframe src='https://clips.twitch.tv/embed?clip=PoliteApatheticAlligatorMingLee&tt_medium=clips_api&tt_content=embed' width='640' height='360' frameborder='0' scrolling='no' allowfullscreen='true'><\/iframe>","broadcaster":{"id":"12826","name":"twitch","display_name":"Twitch","channel_url":"https://www.twitch.tv/twitch","logo":"https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-profile_image-8a8c5be2e3b64a9a-300x300.png"},"curator":{"id":"149747285","name":"twitchpresents","display_name":"TwitchPresents","channel_url":"https://www.twitch.tv/twitchpresents","logo":"https://static-cdn.jtvnw.net/jtv_user_pictures/twitchpresents-profile_image-ca5623eef596a5ee-300x300.jpeg"},"vod":{"id":"130913659","url":"https://www.twitch.tv/videos/130913659?t=23m56s"},"game":"Talk Shows","language":"en","title":"Twitch Weekly - LIVE every Friday @ 1pm PT / 4pm ET!","views":762,"duration":60.01,"created_at":"2017-03-26T21:44:51Z","thumbnails":{"medium":"https://clips-media-assets.twitch.tv/vod-130913659-offset-1436.4239999999938-60-preview-480x272.jpg","small":"https://clips-media-assets.twitch.tv/vod-130913659-offset-1436.4239999999938-60-preview-260x147.jpg","tiny":"https://clips-media-assets.twitch.tv/vod-130913659-offset-1436.4239999999938-60-preview-86x45.jpg"}},{"id":"CrepuscularViscousShallotPJSugar","tracking_id":"61817526","url":"https://clips.twitch.tv/CrepuscularViscousShallotPJSugar?tt_medium=clips_api&tt_content=url","embed_url":"https://clips.twitch.tv/embed?clip=CrepuscularViscousShallotPJSugar&tt_medium=clips_api&tt_content=embed","embed_html":"<iframe src='https://clips.twitch.tv/embed?clip=CrepuscularViscousShallotPJSugar&tt_medium=clips_api&tt_content=embed' width='640' height='360' frameborder='0' scrolling='no' allowfullscreen='true'><\/iframe>","broadcaster":{"id":"12826","name":"twitch","display_name":"Twitch","channel_url":"https://www.twitch.tv/twitch","logo":"https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-profile_image-8a8c5be2e3b64a9a-300x300.png"},"curator":{"id":"149747285","name":"twitchpresents","display_name":"TwitchPresents","channel_url":"https://www.twitch.tv/twitchpresents","logo":"https://static-cdn.jtvnw.net/jtv_user_pictures/twitchpresents-profile_image-ca5623eef596a5ee-300x300.jpeg"},"vod":{"id":"130913659","url":"https://www.twitch.tv/videos/130913659?t=24m28s"},"game":"Talk Shows","language":"en","title":"Twitch Weekly - LIVE every Friday @ 1pm PT / 4pm ET!","views":2,"duration":32.031678,"created_at":"2017-03-26T21:43:10Z","thumbnails":{"medium":"https://clips-media-assets.twitch.tv/vod-130913659-offset-1468-preview-480x272.jpg","small":"https://clips-media-assets.twitch.tv/vod-130913659-offset-1468-preview-260x147.jpg","tiny":"https://clips-media-assets.twitch.tv/vod-130913659-offset-1468-preview-86x45.jpg"}}]
     * _cursor : Mg==
     */

    private String _cursor;
    private List<ClipsEntity> clips;

    public String get_cursor() {
        return _cursor;
    }

    public void set_cursor(String _cursor) {
        this._cursor = _cursor;
    }

    public List<ClipsEntity> getClips() {
        return clips;
    }

    public void setClips(List<ClipsEntity> clips) {
        this.clips = clips;
    }

    public static class ClipsEntity {
        /**
         * id : PoliteApatheticAlligatorMingLee
         * tracking_id : 61818131
         * url : https://clips.twitch.tv/PoliteApatheticAlligatorMingLee?tt_medium=clips_api&tt_content=url
         * embed_url : https://clips.twitch.tv/embed?clip=PoliteApatheticAlligatorMingLee&tt_medium=clips_api&tt_content=embed
         * embed_html : <iframe src='https://clips.twitch.tv/embed?clip=PoliteApatheticAlligatorMingLee&tt_medium=clips_api&tt_content=embed' width='640' height='360' frameborder='0' scrolling='no' allowfullscreen='true'></iframe>
         * broadcaster : {"id":"12826","name":"twitch","display_name":"Twitch","channel_url":"https://www.twitch.tv/twitch","logo":"https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-profile_image-8a8c5be2e3b64a9a-300x300.png"}
         * curator : {"id":"149747285","name":"twitchpresents","display_name":"TwitchPresents","channel_url":"https://www.twitch.tv/twitchpresents","logo":"https://static-cdn.jtvnw.net/jtv_user_pictures/twitchpresents-profile_image-ca5623eef596a5ee-300x300.jpeg"}
         * vod : {"id":"130913659","url":"https://www.twitch.tv/videos/130913659?t=23m56s"}
         * game : Talk Shows
         * language : en
         * title : Twitch Weekly - LIVE every Friday @ 1pm PT / 4pm ET!
         * views : 762
         * duration : 60.01
         * created_at : 2017-03-26T21:44:51Z
         * thumbnails : {"medium":"https://clips-media-assets.twitch.tv/vod-130913659-offset-1436.4239999999938-60-preview-480x272.jpg","small":"https://clips-media-assets.twitch.tv/vod-130913659-offset-1436.4239999999938-60-preview-260x147.jpg","tiny":"https://clips-media-assets.twitch.tv/vod-130913659-offset-1436.4239999999938-60-preview-86x45.jpg"}
         */

        private String id;
        private String tracking_id;
        private String url;
        private String embed_url;
        private String embed_html;
        private BroadcasterEntity broadcaster;
        private CuratorEntity curator;
        private VodEntity vod;
        private String game;
        private String language;
        private String title;
        private int views;
        private double duration;
        private String created_at;
        private ThumbnailsEntity thumbnails;
        private boolean followed;
        private List<VideoEntity> quality_options;
        private int playProgress;
        private ChatEntity chatEntity;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTracking_id() {
            return tracking_id;
        }

        public void setTracking_id(String tracking_id) {
            this.tracking_id = tracking_id;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getEmbed_url() {
            return embed_url;
        }

        public void setEmbed_url(String embed_url) {
            this.embed_url = embed_url;
        }

        public String getEmbed_html() {
            return embed_html;
        }

        public void setEmbed_html(String embed_html) {
            this.embed_html = embed_html;
        }

        public BroadcasterEntity getBroadcaster() {
            return broadcaster;
        }

        public void setBroadcaster(BroadcasterEntity broadcaster) {
            this.broadcaster = broadcaster;
        }

        public CuratorEntity getCurator() {
            return curator;
        }

        public void setCurator(CuratorEntity curator) {
            this.curator = curator;
        }

        public VodEntity getVod() {
            return vod;
        }

        public void setVod(VodEntity vod) {
            this.vod = vod;
        }

        public String getGame() {
            return game;
        }

        public void setGame(String game) {
            this.game = game;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getViews() {
            return views;
        }

        public void setViews(int views) {
            this.views = views;
        }

        public double getDuration() {
            return duration;
        }

        public void setDuration(double duration) {
            this.duration = duration;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public ThumbnailsEntity getThumbnails() {
            return thumbnails;
        }

        public void setThumbnails(ThumbnailsEntity thumbnails) {
            this.thumbnails = thumbnails;
        }

        public boolean isFollowed() {
            return followed;
        }

        public void setFollowed(boolean followed) {
            this.followed = followed;
        }

        public List<VideoEntity> getQuality_options() {
            return quality_options;
        }

        public void setQuality_options(List<VideoEntity> quality_options) {
            this.quality_options = quality_options;
        }

        public ChatEntity getChatEntity() {
            return chatEntity;
        }

        public void setChatEntity(ChatEntity chatEntity) {
            this.chatEntity = chatEntity;
        }

        public int getPlayProgress() {
            return playProgress;
        }

        public void setPlayProgress(int playProgress) {
            this.playProgress = playProgress;
        }

        public static class BroadcasterEntity {
            /**
             * id : 12826
             * name : twitch
             * display_name : Twitch
             * channel_url : https://www.twitch.tv/twitch
             * logo : https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-profile_image-8a8c5be2e3b64a9a-300x300.png
             */

            private String id;
            private String name;
            private String display_name;
            private String channel_url;
            private String logo;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDisplay_name() {
                return display_name;
            }

            public void setDisplay_name(String display_name) {
                this.display_name = display_name;
            }

            public String getChannel_url() {
                return channel_url;
            }

            public void setChannel_url(String channel_url) {
                this.channel_url = channel_url;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }

        public static class CuratorEntity {
            /**
             * id : 149747285
             * name : twitchpresents
             * display_name : TwitchPresents
             * channel_url : https://www.twitch.tv/twitchpresents
             * logo : https://static-cdn.jtvnw.net/jtv_user_pictures/twitchpresents-profile_image-ca5623eef596a5ee-300x300.jpeg
             */

            private String id;
            private String name;
            private String display_name;
            private String channel_url;
            private String logo;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDisplay_name() {
                return display_name;
            }

            public void setDisplay_name(String display_name) {
                this.display_name = display_name;
            }

            public String getChannel_url() {
                return channel_url;
            }

            public void setChannel_url(String channel_url) {
                this.channel_url = channel_url;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }

        public static class VodEntity {
            /**
             * id : 130913659
             * url : https://www.twitch.tv/videos/130913659?t=23m56s
             */

            private String id;
            private String url;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }

        public static class ThumbnailsEntity {
            /**
             * medium : https://clips-media-assets.twitch.tv/vod-130913659-offset-1436.4239999999938-60-preview-480x272.jpg
             * small : https://clips-media-assets.twitch.tv/vod-130913659-offset-1436.4239999999938-60-preview-260x147.jpg
             * tiny : https://clips-media-assets.twitch.tv/vod-130913659-offset-1436.4239999999938-60-preview-86x45.jpg
             */

            private String medium;
            private String small;
            private String tiny;

            public String getMedium() {
                return medium;
            }

            public void setMedium(String medium) {
                this.medium = medium;
            }

            public String getSmall() {
                return small;
            }

            public void setSmall(String small) {
                this.small = small;
            }

            public String getTiny() {
                return tiny;
            }

            public void setTiny(String tiny) {
                this.tiny = tiny;
            }
        }

        public static class VideoEntity {

            private String quality;
            private String source;

            public String getQuality() {
                return quality;
            }

            public void setQuality(String quality) {
                this.quality = quality;
            }

            public String getSource() {
                return source;
            }

            public void setSource(String source) {
                this.source = source;
            }
        }

        public static class ChatEntity implements Serializable{

            private static final long serialVersionUID = 1L;
            private MetaEntity meta;
            private List<DataEntity> data;

            public MetaEntity getMeta() {
                return meta;
            }

            public void setMeta(MetaEntity meta) {
                this.meta = meta;
            }

            public List<DataEntity> getData() {
                return data;
            }

            public void setData(List<DataEntity> data) {
                this.data = data;
            }

            public static class MetaEntity implements Serializable {
                /**
                 * next : null
                 */

                private static final long serialVersionUID = 2L;

                private Object next;

                public Object getNext() {
                    return next;
                }

                public void setNext(Object next) {
                    this.next = next;
                }
            }

            public static class DataEntity implements Serializable {
                /**
                 * type : rechat-message
                 * id : chat-15-2017:AVtdP55GbHT0p-8B5CTt
                 * attributes : {"command":"","room":"playhearthstone","timestamp":1491918086560,"video-offset":23003560,"deleted":false,"message":"sjowE Yuicy Brusi up next sjowW","from":"blackfyre967","tags":{"badges":"premium/1","color":"#201622","display-name":"blackfyre967","emotes":{"45509":[[26,30]],"113958":[[0,4]]},"id":"26c11ebe-3bba-4bb3-ad21-bc194ca87d5e","mod":false,"room-id":"42776357","sent-ts":"1491918103231","subscriber":false,"tmi-sent-ts":"1491918100955","turbo":false,"user-id":"106532385","user-type":null},"color":"#201622"}
                 * links : {"self":"/rechat-message/chat-15-2017:AVtdP55GbHT0p-8B5CTt"}
                 */

                private static final long serialVersionUID = 3L;

                private String type;
                private String id;
                private AttributesEntity attributes;
                private LinksEntity links;

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public AttributesEntity getAttributes() {
                    return attributes;
                }

                public void setAttributes(AttributesEntity attributes) {
                    this.attributes = attributes;
                }

                public LinksEntity getLinks() {
                    return links;
                }

                public void setLinks(LinksEntity links) {
                    this.links = links;
                }

                public static class AttributesEntity implements Serializable {
                    /**
                     * command :
                     * room : playhearthstone
                     * timestamp : 1491918086560
                     * video-offset : 23003560
                     * deleted : false
                     * message : sjowE Yuicy Brusi up next sjowW
                     * from : blackfyre967
                     * tags : {"badges":"premium/1","color":"#201622","display-name":"blackfyre967","emotes":{"45509":[[26,30]],"113958":[[0,4]]},"id":"26c11ebe-3bba-4bb3-ad21-bc194ca87d5e","mod":false,"room-id":"42776357","sent-ts":"1491918103231","subscriber":false,"tmi-sent-ts":"1491918100955","turbo":false,"user-id":"106532385","user-type":null}
                     * color : #201622
                     */

                    private static final long serialVersionUID = 5L;

                    private String command;
                    private String room;
                    private long timestamp;
                    @SerializedName("video-offset")
                    private int videooffset;
                    private boolean deleted;
                    private String message;
                    private String from;
                    private TagsEntity tags;
                    private String color;

                    public String getCommand() {
                        return command;
                    }

                    public void setCommand(String command) {
                        this.command = command;
                    }

                    public String getRoom() {
                        return room;
                    }

                    public void setRoom(String room) {
                        this.room = room;
                    }

                    public long getTimestamp() {
                        return timestamp;
                    }

                    public void setTimestamp(long timestamp) {
                        this.timestamp = timestamp;
                    }

                    public int getVideooffset() {
                        return videooffset;
                    }

                    public void setVideooffset(int videooffset) {
                        this.videooffset = videooffset;
                    }

                    public boolean isDeleted() {
                        return deleted;
                    }

                    public void setDeleted(boolean deleted) {
                        this.deleted = deleted;
                    }

                    public String getMessage() {
                        return message;
                    }

                    public void setMessage(String message) {
                        this.message = message;
                    }

                    public String getFrom() {
                        return from;
                    }

                    public void setFrom(String from) {
                        this.from = from;
                    }

                    public TagsEntity getTags() {
                        return tags;
                    }

                    public void setTags(TagsEntity tags) {
                        this.tags = tags;
                    }

                    public String getColor() {
                        return color;
                    }

                    public void setColor(String color) {
                        this.color = color;
                    }

                    public static class TagsEntity implements Serializable {
                        /**
                         * badges : premium/1
                         * color : #201622
                         * display-name : blackfyre967
                         * emotes : {"45509":[[26,30]],"113958":[[0,4]]}
                         * id : 26c11ebe-3bba-4bb3-ad21-bc194ca87d5e
                         * mod : false
                         * room-id : 42776357
                         * sent-ts : 1491918103231
                         * subscriber : false
                         * tmi-sent-ts : 1491918100955
                         * turbo : false
                         * user-id : 106532385
                         * user-type : null
                         */

                        private static final long serialVersionUID = 6L;

                        private String badges;
                        private String color;
                        @SerializedName("display-name")
                        private String displayname;
                        private EmotesEntity emotes;
                        private String id;
                        private boolean mod;
                        @SerializedName("room-id")
                        private String roomid;
                        @SerializedName("sent-ts")
                        private String sentts;
                        private boolean subscriber;
                        @SerializedName("tmi-sent-ts")
                        private String tmisentts;
                        private boolean turbo;
                        @SerializedName("user-id")
                        private String userid;
                        @SerializedName("user-type")
                        private Object usertype;

                        public String getBadges() {
                            return badges;
                        }

                        public void setBadges(String badges) {
                            this.badges = badges;
                        }

                        public String getColor() {
                            return color;
                        }

                        public void setColor(String color) {
                            this.color = color;
                        }

                        public String getDisplayname() {
                            return displayname;
                        }

                        public void setDisplayname(String displayname) {
                            this.displayname = displayname;
                        }

                        public EmotesEntity getEmotes() {
                            return emotes;
                        }

                        public void setEmotes(EmotesEntity emotes) {
                            this.emotes = emotes;
                        }

                        public String getId() {
                            return id;
                        }

                        public void setId(String id) {
                            this.id = id;
                        }

                        public boolean isMod() {
                            return mod;
                        }

                        public void setMod(boolean mod) {
                            this.mod = mod;
                        }

                        public String getRoomid() {
                            return roomid;
                        }

                        public void setRoomid(String roomid) {
                            this.roomid = roomid;
                        }

                        public String getSentts() {
                            return sentts;
                        }

                        public void setSentts(String sentts) {
                            this.sentts = sentts;
                        }

                        public boolean isSubscriber() {
                            return subscriber;
                        }

                        public void setSubscriber(boolean subscriber) {
                            this.subscriber = subscriber;
                        }

                        public String getTmisentts() {
                            return tmisentts;
                        }

                        public void setTmisentts(String tmisentts) {
                            this.tmisentts = tmisentts;
                        }

                        public boolean isTurbo() {
                            return turbo;
                        }

                        public void setTurbo(boolean turbo) {
                            this.turbo = turbo;
                        }

                        public String getUserid() {
                            return userid;
                        }

                        public void setUserid(String userid) {
                            this.userid = userid;
                        }

                        public Object getUsertype() {
                            return usertype;
                        }

                        public void setUsertype(Object usertype) {
                            this.usertype = usertype;
                        }

                        public static class EmotesEntity implements Serializable {

                            private static final long serialVersionUID = 7L;
                            @SerializedName("45509")
                            private List<List<Integer>> _$45509;
                            @SerializedName("113958")
                            private List<List<Integer>> _$113958;

                            public List<List<Integer>> get_$45509() {
                                return _$45509;
                            }

                            public void set_$45509(List<List<Integer>> _$45509) {
                                this._$45509 = _$45509;
                            }

                            public List<List<Integer>> get_$113958() {
                                return _$113958;
                            }

                            public void set_$113958(List<List<Integer>> _$113958) {
                                this._$113958 = _$113958;
                            }
                        }
                    }
                }

                public static class LinksEntity implements Serializable {
                    /**
                     * self : /rechat-message/chat-15-2017:AVtdP55GbHT0p-8B5CTt
                     */

                    private static final long serialVersionUID = 4L;

                    private String self;

                    public String getSelf() {
                        return self;
                    }

                    public void setSelf(String self) {
                        this.self = self;
                    }
                }
            }
        }
    }
}
