package com.battlecryinc.tclips.clips.presenter.clips;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.battlecryinc.tclips.baselibrary.rx.RxBus;
import com.battlecryinc.tclips.clips.R;
import com.battlecryinc.tclips.clips.bean.Clip;
import com.battlecryinc.tclips.clips.bean.FollowChannel;
import com.battlecryinc.tclips.clips.bean.NavMenu;
import com.battlecryinc.tclips.clips.bean.TwitchGame;
import com.battlecryinc.tclips.clips.contract.ClipsContract;
import com.battlecryinc.tclips.clips.delegate.ClipCommentDelegate;
import com.battlecryinc.tclips.clips.delegate.ClipViewDelegate;
import com.battlecryinc.tclips.clips.delegate.NavMenuDelegate;
import com.battlecryinc.tclips.clips.listener.OnClipViewClickListener;
import com.battlecryinc.tclips.clips.retrofit.restadapter.RestAdapter;
import com.battlecryinc.tclips.clips.retrofit.service.TwitchClipsService;
import com.battlecryinc.tclips.clips.service.GetFChannelService;
import com.battlecryinc.tclips.clips.util.SerializableUtil;
import com.battlecryinc.tclips.clips.view.fullscreenvideo.FullScreenVideoActivity;
import com.battlecryinc.tclips.router.BuildConfig;
import com.battlecryinc.tclips.router.activityhelper.ActivityLaunchHelper;
import com.battlecryinc.tclips.router.activityhelper.ActivityList;
import com.battlecryinc.tclips.router.db.DB;
import com.battlecryinc.tclips.router.db.table.User;
import com.battlecryinc.tclips.router.db.table.UserDao;
import com.battlecryinc.tclips.router.retrofit.HttpServiceUtil;
import com.battlecryinc.tclips.router.rxmsg.RxGetFChannel;
import com.battlecryinc.tclips.router.rxmsg.RxLoginReport;

import org.greenrobot.greendao.query.QueryBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import co.lujun.buildingblocks.BaseAdapter;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

import static android.widget.AdapterView.OnItemClickListener;
import static com.battlecryinc.tclips.clips.bean.Clip.ClipsEntity;
import static com.battlecryinc.tclips.clips.bean.NavMenu.NavMenuEntity;
import static com.battlecryinc.tclips.clips.bean.TwitchGame.TopEntity;
import static com.battlecryinc.tclips.clips.listener.OnClipViewClickListener.CLIP_MENU_T;

/**
 * Created by geoff on 17/3/28.
 */

public class ClipsActivityPresenter implements ClipsContract.IClipsPresenter {

    private List mData, mNavMenu;
    private BaseAdapter mNavMenuAdapter;
    private BaseAdapter mAdapter;
    private AdapterView.OnItemClickListener mOnItemClickListener;
    private Runnable mInvalidateMenuRunnable;
    private UserDao mUserDao;
    private OnClipViewClickListener mClipViewClickListener;
    private OnItemClickListener mOnVideoLoadListener;
    private List<String> mFollowedChannels;
    private OkHttpClient mTwitchTokenClient;
    private Toast reqTimeOutToast;
    private ClipViewDelegate mClipViewDelegate;

    private String mRequestClipsGames, mFollowedChannel, mReqPeriod;
    private boolean mReqTrending;
    private int mLastNavPosition;

    private ClipsContract.IClipsView iClipsView;
    private Context appContext;

    @Inject
    public ClipsActivityPresenter(ClipsContract.IClipsView view){
        iClipsView = view;
    }

    @Override
    public void start() {
        appContext = iClipsView.getActivity().getApplicationContext();
        mUserDao = DB.get().getDaoSession().getUserDao();
        mData = new ArrayList();
        mNavMenu = new ArrayList();
        mFollowedChannels = new ArrayList<String>();
        reqTimeOutToast = Toast.makeText(appContext, R.string.clips_request_timeout, Toast.LENGTH_SHORT);
        mInvalidateMenuRunnable = new Runnable() {
            @Override
            public void run() {
                invalidateNavMenu();
            }
        };
        mOnItemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < mNavMenu.size(); i++) {
                    ((NavMenu) mNavMenu.get(i)).setChecked(i == position);
                }

                NavMenu menu = ((NavMenu) mNavMenu.get(position));
                mReqPeriod = "week";
                if (menu.getType() == NavMenu.MENU_T.TWITCH_GAME){
                    mFollowedChannel = null;
                    mRequestClipsGames = menu.getTopEntity().getGame().getName();
                }else {
                    mRequestClipsGames = null;
                    if (menu.getBMenuT() == NavMenu.B_MENU_T.FOLLOW_CLIPS){
                        mFollowedChannel = getLoginAccount().getFollowedChannel() == null ?
                                "" : getLoginAccount().getFollowedChannel();
                    }else if (menu.getBMenuT() == NavMenu.B_MENU_T.MY_CLIP){
                        mReqPeriod = "all";
                        mFollowedChannel = getLoginAccount().getName();
                    }else {
                        mFollowedChannel = null;
                    }
                }

                iClipsView.closeDrawer(true);
                iClipsView.post(mInvalidateMenuRunnable, 1000);
                if (mLastNavPosition != position) {
                    mLastNavPosition = position;
                    mData.clear();
                    invalidateClips();
                    doRequestData();
                }
            }
        };
        mClipViewClickListener = new OnClipViewClickListener() {
            @Override
            public void onClipViewClick(int position, CLIP_MENU_T type) {
                onClipItemClick(position, type);
            }
        };
        mOnVideoLoadListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mAdapter.notifyItemChanged(position);
            }
        };

        mClipViewDelegate = new ClipViewDelegate(mClipViewClickListener, mOnVideoLoadListener);
        mAdapter = new BaseAdapter()
                .addDelegate(mClipViewDelegate)
                .addDelegate(new ClipCommentDelegate())
                .buildWith(mData);
        mNavMenuAdapter = new BaseAdapter()
                .addDelegate(new NavMenuDelegate(mOnItemClickListener))
                .buildWith(mNavMenu);
        iClipsView.setRecyclerViewAdapter(mAdapter);
        iClipsView.setNavAdapter(mNavMenuAdapter);

        initRx();
        initLoginAccount();
        validateFollowedChannels();
        readData();
        initNavMenu();
        doRequestData();
    }

    @Override
    public void onListScroll(int firstCompletelyVisibleItemPos){
        if (mClipViewDelegate.getCurrentFocus() != firstCompletelyVisibleItemPos){
            mClipViewDelegate.setCurrentFocus(firstCompletelyVisibleItemPos);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void requestData(){
        RestAdapter.get().getTwitchClipsService()
                .getTopClips(100, mRequestClipsGames, mFollowedChannel, mReqPeriod, mReqTrending)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Clip>() {
                    @Override
                    public void accept(@NonNull Clip clip) throws Exception {
                        iClipsView.setClipsRefreshing(false);
                        if (clip == null){
                            return;
                        }
                        saveData(clip);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        iClipsView.setClipsRefreshing(false);
                    }
                });
    }

    @Override
    public void login(){
        iClipsView.closeDrawer(true);
        ActivityLaunchHelper.startActivityForName(iClipsView.getActivity(), ActivityList.ATY_AUTH);
    }

    @Override
    public void reloadTopGames(){
        initNavMenu();
    }

    @Override
    public void logout(){
        if (mUserDao.queryBuilder().where(UserDao.Properties.CurrentLogin.eq(true)).count() <= 0){
            Toast.makeText(appContext, R.string.clips_no_login_account, Toast.LENGTH_SHORT).show();
            return;
        }
        List<User> users = mUserDao.queryBuilder().list();
        for (User user : users) {
            if (user.getCurrentLogin()) user.setCurrentLogin(false);
        }
        mUserDao.updateInTx(users);
        RxBus.get().post(new RxLoginReport(false));
    }

    @Override
    public void aboutApp(){
        try {
            PackageInfo info = appContext.getPackageManager()
                    .getPackageInfo(appContext.getPackageName(), 0);
            new AlertDialog.Builder(iClipsView.getActivity()).setTitle(R.string.clips_action_about)
                    .setMessage("v" + info.versionName + ", build " + info.versionCode)
                    .setPositiveButton(android.R.string.yes,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {}
                            })
                    .show();
        }catch (PackageManager.NameNotFoundException e){}
    }

    private void resetReqParams(){
        mRequestClipsGames = mFollowedChannel = null;
        mReqPeriod = "week";
    }

    private void initLoginAccount(){
        User user;
        if ((user = getLoginAccount()) != null){
            Intent intent = new Intent(iClipsView.getActivity(), GetFChannelService.class);
            intent.putExtra(GetFChannelService.GET_FC_UID_EXTRA, String.valueOf(user.get_id()));
            iClipsView.getActivity().startService(intent);
        }
    }

    private void initRx(){
        RxBus.get().toFlowable(RxLoginReport.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<RxLoginReport>() {
                    @Override
                    public void accept(@NonNull RxLoginReport rxLoginReport) throws Exception {
                        User user;
                        if ((user = getLoginAccount()) == null){
                            iClipsView.invalidateNavHeaderView(false, null, null, null);
                        }else {
                            iClipsView.invalidateNavHeaderView(true, user.getLogo(),
                                    user.getName(), user.getBio());
                            initLoginAccount();
                        }
                        validateFollowedChannels();
                        initUserMenu();
                        resetReqParams();
                        doRequestData();
                    }
                });
        RxBus.get().toFlowable(RxGetFChannel.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<RxGetFChannel>() {
                    @Override
                    public void accept(@NonNull RxGetFChannel rxGetFChannel) throws Exception {
                        validateFollowedChannels();
                        reAssembleData();
                        invalidateClips();
                    }
                });
    }

    private void doRequestData(){
        if (!iClipsView.getClipsRefreshing()){
            iClipsView.setClipsRefreshing(true);
        }
        requestData();
    }

    private void invalidateNavMenu(){
        if (mNavMenuAdapter == null){
            return;
        }
        mNavMenuAdapter.notifyDataSetChanged();
    }

    private void invalidateClips(){
        if (mAdapter == null){
            return;
        }
        mAdapter.notifyDataSetChanged();
    }

    private void readData(){
//        showData();
    }

    private void saveData(Clip clip){
        readData();
        // test
        showData(clip);
    }

    private void showData(Clip clip){
        mData.clear();

        for (ClipsEntity entity : clip.getClips()) {
            mData.add(entity);
        }
        reAssembleData();

        invalidateClips();
        onListScroll(0);
    }

    private void reAssembleData(){
        for (Object obj : mData) {
            if (obj instanceof ClipsEntity){
                ClipsEntity entity = (ClipsEntity) obj;
                entity.setFollowed(mFollowedChannels.contains(entity.getBroadcaster().getName()));
            }
        }
    }

    private void initNavMenu(){
        User user;
        if ((user = getLoginAccount()) != null){
            iClipsView.invalidateNavHeaderView(true, user.getLogo(), user.getName(), user.getBio());
        }else {
            iClipsView.invalidateNavHeaderView(false, null, null, null);
        }
        final String popMenuStr = appContext.getString(R.string.clips_menu_title_popular_games);
        iClipsView.setNavProgressBarVisible(true, false);
        RestAdapter.get().getTwitchClipsService().getTopGames(50, 0)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<TwitchGame>() {
                    @Override
                    public void accept(@NonNull TwitchGame twitchGame) throws Exception {
                        iClipsView.setNavProgressBarVisible(false, false);
                        if (twitchGame == null || twitchGame.getTop() == null){
                            return;
                        }
                        mNavMenu.clear();
                        for (TopEntity entity : twitchGame.getTop()) {
                            NavMenu menu = new NavMenu();
                            menu.setType(NavMenu.MENU_T.TWITCH_GAME);
                            menu.setTopEntity(entity);
                            menu.setTitle(popMenuStr);
                            mNavMenu.add(menu);
                        }
                        initUserMenu();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        iClipsView.setNavProgressBarVisible(false, true);
                    }
                });
    }

    private void initUserMenu(){
        final String browseTitle = appContext.getString(R.string.clips_menu_title_browse);

        // remove all BROWSE menu first
        Iterator iterator = mNavMenu.iterator();
        while (iterator.hasNext()){
            Object obj = iterator.next();
            if (obj instanceof NavMenu && ((NavMenu) obj).getType() == NavMenu.MENU_T.BROWSE) {
                iterator.remove();
            }
        }

        User user = getLoginAccount();
        // my clips
        if (user != null){
            NavMenuEntity likeEntity = new NavMenuEntity();
            likeEntity.setIcon(R.drawable.clips_ic_nav_menu_like_white);
            likeEntity.setContent(appContext.getString(R.string.clips_menu_like_clips));

            NavMenu likeMenu = new NavMenu();
            likeMenu.setType(NavMenu.MENU_T.BROWSE);
            likeMenu.setNavMenuEntity(likeEntity);
            likeMenu.setTitle(browseTitle);
            likeMenu.setChecked(false);
            likeMenu.setBMenuT(NavMenu.B_MENU_T.MY_CLIP);

            mNavMenu.add(0, likeMenu);
        }

        // clips menu
        NavMenuEntity entity = new NavMenuEntity();
        entity.setIcon(R.drawable.clips_ic_nav_menu_clips_white);
        entity.setContent(appContext.getString(R.string.clips_menu_clips));

        NavMenu clipMenu = new NavMenu();
        clipMenu.setType(NavMenu.MENU_T.BROWSE);
        clipMenu.setNavMenuEntity(entity);
        clipMenu.setTitle(browseTitle);
        clipMenu.setChecked(true);
        clipMenu.setBMenuT(NavMenu.B_MENU_T.CLIPS);

        mNavMenu.add(user != null ? 1 : 0, clipMenu);
        mLastNavPosition = user != null ? 1 : 0;

        // my followed channel clips
        if (user != null/* && !TextUtils.isEmpty(user.getFollowedChannel())*/){
            NavMenuEntity fcEntity = new NavMenuEntity();
            fcEntity.setIcon(R.drawable.clips_ic_nav_menu_game_white);
            fcEntity.setContent(appContext.getString(R.string.clips_menu_f_channel_clips));

            NavMenu fcMenu = new NavMenu();
            fcMenu.setType(NavMenu.MENU_T.BROWSE);
            fcMenu.setNavMenuEntity(fcEntity);
            fcMenu.setTitle(browseTitle);
            fcMenu.setChecked(false);
            fcMenu.setBMenuT(NavMenu.B_MENU_T.FOLLOW_CLIPS);

            mNavMenu.add(2, fcMenu);
        }

        invalidateNavMenu();
    }

    private User getLoginAccount(){
        QueryBuilder<User> userQuery = mUserDao.queryBuilder().where(
                UserDao.Properties.CurrentLogin.eq(true));
        if (userQuery.count() > 0){
            return userQuery.list().get(0);
        }
        return null;
    }

    private void validateFollowedChannels(){
        mFollowedChannels.clear();
        User user = getLoginAccount();
        if (user != null && user.getFollowedChannel() != null){
            mFollowedChannels.addAll(Arrays.asList(SerializableUtil.readFChannelObject(
                    appContext, String.valueOf(user.get_id())).toString().split(",")));
        }
    }

    private void onClipItemClick(int pos, CLIP_MENU_T type){
        if (!(mData.get(pos) instanceof ClipsEntity)){
            return;
        }
        ClipsEntity entity = (ClipsEntity) mData.get(pos);
        switch (type){
            case CLIP_VIEW_AVATAR:
                show1ChannelClips(entity);
                break;

            case CLIP_VIEW_FOLLOW:
                followChannel(entity);
                break;

            case CLIP_VIEW_3P:
                on3PMenuClick(entity);
                break;

            case CLIP_VIEW_FULLSCREEN:
                onFullScreenVideo(entity);
                break;

            case CLIP_VIEW_SHARE:
                onShareClip(entity);
                break;

            case CLIP_VIEW_SCROLL_2_1:
                scrollClip21(pos);
                break;
        }
    }

    private void scrollClip21(int pos){
        iClipsView.scrollList(pos);
    }

    private void onShareClip(ClipsEntity entity){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, entity.getEmbed_url());
        sendIntent.setType("text/plain");
        iClipsView.getActivity().startActivity(Intent.createChooser(sendIntent,
                appContext.getString(R.string.clips_action_share)));
    }

    private void onFullScreenVideo(ClipsEntity entity){
        Intent intent = new Intent(iClipsView.getActivity(), FullScreenVideoActivity.class);
        intent.putExtra("url", entity.getQuality_options().get(0).getSource());
        intent.putExtra("title", entity.getTitle());
        intent.putExtra("progress", entity.getPlayProgress());

        Bundle bundle = new Bundle();
        bundle.putSerializable("chat_e", entity.getChatEntity());
        bundle.putString("vod_url", entity.getVod() == null ? "" : entity.getVod().getUrl());
        intent.putExtras(bundle);

        iClipsView.getActivity().startActivity(intent);
    }

    private void on3PMenuClick(final ClipsEntity entity){
        String copyLink = appContext.getString(R.string.clips_copy);
        String report = appContext.getString(R.string.clips_report);
        new AlertDialog.Builder(iClipsView.getActivity())
                .setItems(new String[]{copyLink, report}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                ClipboardManager clipboardManager = (ClipboardManager)
                                        appContext.getSystemService(Context.CLIPBOARD_SERVICE);
                                ClipData data = ClipData.newPlainText("tclips-data", entity.getEmbed_url());
                                clipboardManager.setPrimaryClip(data);
                                Toast.makeText(appContext, R.string.clips_action_copied, Toast.LENGTH_SHORT).show();
                                break;

                            case 1:
                                new AlertDialog.Builder(iClipsView.getActivity())
                                        .setMessage(R.string.clips_report_text)
                                        .setPositiveButton(android.R.string.yes,
                                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).show();
                                break;
                        }
                    }
                }).show();
    }

    private void followChannel(final ClipsEntity entity){
        final User user;
        if ((user = getLoginAccount()) == null){
            Toast.makeText(appContext, R.string.clips_no_login_account, Toast.LENGTH_SHORT).show();
            return;
        }
        final String uid = String.valueOf(user.get_id());
        iClipsView.setListProgressbarVisible(true);
        if (entity.isFollowed()){
            HttpServiceUtil.getCustomClientService(TwitchClipsService.class, getTwitchTokenClient(user))
                    .unFollowChannel(uid, entity.getBroadcaster().getId())
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<retrofit2.Response<Integer>>() {
                        @Override
                        public void accept(@NonNull retrofit2.Response<Integer> response) throws Exception {
                            iClipsView.setListProgressbarVisible(false);
                            if (response.code() == 204) {
                                followChannelLocal(uid, entity.getBroadcaster().getName(), false);
                            }else {
                                reqTimeOutToast.show();
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(@NonNull Throwable throwable) throws Exception {
                            iClipsView.setListProgressbarVisible(false);
                            reqTimeOutToast.show();
                        }
                    });
        }else {
            HttpServiceUtil.getCustomClientService(TwitchClipsService.class, getTwitchTokenClient(user))
                    .followChannel(String.valueOf(user.get_id()), entity.getBroadcaster().getId())
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<FollowChannel>() {
                        @Override
                        public void accept(@NonNull FollowChannel followChannel) throws Exception {
                            iClipsView.setListProgressbarVisible(false);
                            if (followChannel != null && followChannel.getCreated_at() != null) {
                                followChannelLocal(uid, entity.getBroadcaster().getName(), true);
                            }else {
                                reqTimeOutToast.show();
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(@NonNull Throwable throwable) throws Exception {
                            iClipsView.setListProgressbarVisible(false);
                            reqTimeOutToast.show();
                        }
                    });
        }
    }

    private void show1ChannelClips(ClipsEntity entity){
        mReqPeriod = "all";
        mRequestClipsGames = null;
        mFollowedChannel = entity.getBroadcaster().getName();
        mLastNavPosition = -1;

        mData.clear();
        invalidateClips();
        doRequestData();
    }

    private void followChannelLocal(String uid, String channelName, boolean follow){
        if (follow){
            mFollowedChannels.add(channelName);
        }else {
            Iterator<String> iterator = mFollowedChannels.iterator();
            while (iterator.hasNext()){
                String name = iterator.next();
                if (name.equals(channelName)){
                    iterator.remove();
                }
            }
        }

        Observable.just(uid).map(new Function<String, Boolean>() {
            @Override
            public Boolean apply(@NonNull String s) throws Exception {
                StringBuilder builder = new StringBuilder();
                StringBuilder top10Builder = new StringBuilder();
                for (int i = 0; i < mFollowedChannels.size(); i++) {
                    builder.append(mFollowedChannels.get(i) + ",");
                    if (i < 10){
                        top10Builder.append(mFollowedChannels.get(i) + ",");
                    }
                }
                if (mFollowedChannels.size() > 0){
                    builder.deleteCharAt(builder.length() - 1);
                    top10Builder.deleteCharAt(top10Builder.length() - 1);
                }
                SerializableUtil.saveFChannelObject(appContext, builder.toString(), s);
                QueryBuilder<User> userQuery = mUserDao.queryBuilder()
                        .where(UserDao.Properties._id.eq(s));
                if (mUserDao.count() > 0){
                    userQuery.list().get(0).setFollowedChannel(top10Builder.toString());
                    mUserDao.updateInTx(userQuery.list().get(0));
                }
                return true;
            }
        }).subscribeOn(Schedulers.io()).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(@NonNull Boolean aBoolean) throws Exception {}
        });

        reAssembleData();
        invalidateClips();
        iClipsView.showSnackBar(follow ? R.string.clips_followed : R.string.clips_unfollowed);
    }

    private OkHttpClient getTwitchTokenClient(final User user){
        if (user == null){
            return null;
        }
        if (mTwitchTokenClient == null){
            Interceptor authInterceptor = new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request originReq = chain.request();
                    Request curReq = originReq.newBuilder()
                            .header("Authorization", "OAuth " + user.getTwitchToken())
                            .method(originReq.method(), originReq.body())
                            .build();
                    return chain.proceed(curReq);
                }
            };
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY :
                    HttpLoggingInterceptor.Level.NONE);

            mTwitchTokenClient = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .addInterceptor(loggingInterceptor)
                    .addInterceptor(authInterceptor)
                    .build();
        }
        return mTwitchTokenClient;
    }
}
