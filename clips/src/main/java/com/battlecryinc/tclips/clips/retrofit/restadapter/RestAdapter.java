package com.battlecryinc.tclips.clips.retrofit.restadapter;

import com.battlecryinc.tclips.clips.retrofit.service.TClipsService;
import com.battlecryinc.tclips.clips.retrofit.service.TwitchClipsService;
import com.battlecryinc.tclips.router.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by geoff on 17/3/24.
 */

public class RestAdapter {

    private TwitchClipsService twitchClipsService;
    private TClipsService tClipsService;

    public TwitchClipsService getTwitchClipsService() {
        return twitchClipsService;
    }

    public TClipsService getTClipsService() {
        return tClipsService;
    }

    private RestAdapter(){
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY :
                HttpLoggingInterceptor.Level.NONE);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .build();

        twitchClipsService = new Retrofit.Builder()
                .baseUrl(BuildConfig.TWITCH_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
                .create(TwitchClipsService.class);

        tClipsService = new Retrofit.Builder()
                .baseUrl(BuildConfig.TCLIPS_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
                .create(TClipsService.class);
    }

    private static class Holder{

        private static RestAdapter clipsRestAdapter = new RestAdapter();
    }

    public static RestAdapter get(){
        return Holder.clipsRestAdapter;
    }
}
