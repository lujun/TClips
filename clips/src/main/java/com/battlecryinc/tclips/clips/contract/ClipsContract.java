package com.battlecryinc.tclips.clips.contract;

import android.app.Activity;

import com.battlecryinc.tclips.baselibrary.mvp.BasePresenter;
import com.battlecryinc.tclips.baselibrary.mvp.BaseView;

import co.lujun.buildingblocks.BaseAdapter;

/**
 * Created by geoff on 17/4/18.
 */

public interface ClipsContract {

    interface IClipsView extends BaseView<IClipsPresenter> {

        void invalidateNavHeaderView(boolean in, String aUrl, String name, String bio);
        void setRecyclerViewAdapter(BaseAdapter adapter);
        void scrollList(int pos);
        void setNavAdapter(BaseAdapter adapter);
        void setClipsRefreshing(boolean refreshing);
        boolean getClipsRefreshing();
        void setNavProgressBarVisible(boolean visible, boolean reloadShow);
        void closeDrawer(boolean anim);
        void post(Runnable runnable, long timeMills);
        void setListProgressbarVisible(boolean visible);
        void showSnackBar(int resId);
        Activity getActivity();
    }

    interface IClipsPresenter extends BasePresenter {

        void requestData();
        void onListScroll(int firstCompletelyVisibleItemPos);
        void login();
        void logout();
        void reloadTopGames();
        void aboutApp();
    }
}
