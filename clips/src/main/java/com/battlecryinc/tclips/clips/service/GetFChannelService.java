package com.battlecryinc.tclips.clips.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.battlecryinc.tclips.baselibrary.rx.RxBus;
import com.battlecryinc.tclips.clips.bean.FollowedChannel;
import com.battlecryinc.tclips.clips.retrofit.restadapter.RestAdapter;
import com.battlecryinc.tclips.clips.util.SerializableUtil;
import com.battlecryinc.tclips.router.db.DB;
import com.battlecryinc.tclips.router.db.table.User;
import com.battlecryinc.tclips.router.db.table.UserDao;
import com.battlecryinc.tclips.router.rxmsg.RxGetFChannel;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;

import static com.battlecryinc.tclips.clips.bean.FollowedChannel.FollowsEntity;

/**
 * Author: lujun(http://blog.lujun.co)
 * Date: 30/03/2017 22:11
 */

public class GetFChannelService extends IntentService {

    private List<FollowsEntity> followsEntities;

    private String uid;

    private static final String TAG = "GetFChannelService";
    public static final String GET_FC_UID_EXTRA = "get_fc_uid";

    public GetFChannelService(){
        super(TAG);
        followsEntities = new ArrayList<FollowsEntity>();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        uid = intent.getStringExtra(GET_FC_UID_EXTRA);
        onRequestFChannel(0);
    }

    private void onRequestFChannel(int offset){
        Log.d(TAG, "Request user followed channel, offset = " + offset);
        RestAdapter.get().getTwitchClipsService().getFollowedChannel(uid, 100, offset)
                .subscribe(new Consumer<FollowedChannel>() {
                    @Override
                    public void accept(@NonNull FollowedChannel followedChannel) throws Exception {
                        if (followedChannel == null){
                            saveFChannel();
                        }else {
                            followsEntities.addAll(followedChannel.getFollows());
                            if (followsEntities.size() >= followedChannel.get_total()){
                                saveFChannel();
                            }else {
                                onRequestFChannel(followedChannel.getFollows().size());
                            }
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        Log.e(TAG, "Get followed channel error: " + throwable.getMessage());
                        saveFChannel();
                    }
                });
    }

    private void saveFChannel(){
        StringBuilder builder = new StringBuilder();
        StringBuilder top10Builder = new StringBuilder();
        for (int i = 0; i < followsEntities.size(); i++) {
            FollowsEntity entity = followsEntities.get(i);
            builder.append(entity.getChannel().getName() + ",");
            if (i < 10){
                top10Builder.append(entity.getChannel().getName() + ",");
            }
        }
        if (followsEntities.size() > 0){
            builder.deleteCharAt(builder.length() - 1);
            top10Builder.deleteCharAt(top10Builder.length() - 1);
        }
        SerializableUtil.saveFChannelObject(this, builder.toString(), uid);
        UserDao userDao = DB.get().getDaoSession().getUserDao();
        QueryBuilder<User> userQuery = userDao.queryBuilder().where(UserDao.Properties._id.eq(uid));
        if (userDao.count() > 0){
            userQuery.list().get(0).setFollowedChannel(top10Builder.toString());
            userDao.updateInTx(userQuery.list().get(0));
        }
        RxBus.get().post(new RxGetFChannel());
    }
}
