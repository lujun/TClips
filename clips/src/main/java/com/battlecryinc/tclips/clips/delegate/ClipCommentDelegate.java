package com.battlecryinc.tclips.clips.delegate;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.battlecryinc.tclips.clips.R;
import com.battlecryinc.tclips.clips.R2;
import com.battlecryinc.tclips.clips.bean.ClipComment;
import com.battlecryinc.tclips.clips.util.SpannableUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.lujun.buildingblocks.BaseDelegate;

/**
 * Created by geoff on 17/3/27.
 */

public class ClipCommentDelegate extends BaseDelegate<List> {

    @Override
    public boolean isForViewType(@NonNull List items, int position) {
        return items.get(position) instanceof ClipComment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ClipCommentViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.clips_item_clip_comment, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull List items, int position,
                                 @NonNull RecyclerView.ViewHolder holder) {
        ClipCommentViewHolder clipCommentViewHolder = (ClipCommentViewHolder) holder;

        clipCommentViewHolder.etComment.setVisibility(position == items.size() - 1 ||
                !(items.get(position + 1) instanceof ClipComment) ? View.VISIBLE : View.GONE);
        clipCommentViewHolder.tvCommentNum.setVisibility(position > 0 &&
                items.get(position - 1) instanceof ClipComment ? View.GONE : View.VISIBLE);
        clipCommentViewHolder.tvCommentNum.setText("New 8888 comments");
        if (position % 2 == 0) {
            clipCommentViewHolder.tvCommentContent.setText(SpannableUtil.generateCommentLine("geoff", "jack", "Good clip!!!!!!!!!!!!"));
        }else {
            clipCommentViewHolder.tvCommentContent.setText(SpannableUtil.generateCommentLine("Rol", null, "Hahahahahahahahahahahahahahahahahahahahahahahahahahahahahaha!!!!!!!!!!!!"));
        }
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {

    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {

    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {

    }

    class ClipCommentViewHolder extends RecyclerView.ViewHolder{

        @BindView(R2.id.tv_comment_num)
        TextView tvCommentNum;
        @BindView(R2.id.tv_comment_content)
        TextView tvCommentContent;
        @BindView(R2.id.et_comment)
        EditText etComment;

        public ClipCommentViewHolder(View v){
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}