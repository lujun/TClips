package com.battlecryinc.tclips.clips.view.fullscreenvideo;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.WindowManager;

import com.battlecryinc.tclips.baselibrary.util.NetWorkUtil;
import com.battlecryinc.tclips.clips.R;
import com.battlecryinc.tclips.clips.R2;
import com.battlecryinc.tclips.clips.component.DaggerFullScreenVideoComponent;
import com.battlecryinc.tclips.clips.contract.FullScreenVideoContract;
import com.battlecryinc.tclips.clips.module.FullScreenVideoModule;
import com.battlecryinc.tclips.clips.presenter.fullscreenvideo.FullScreenVideoPresenter;
import com.battlecryinc.tclips.router.baseview.BaseActivity;
import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import master.flame.danmaku.controller.DrawHandler;
import master.flame.danmaku.danmaku.model.BaseDanmaku;
import master.flame.danmaku.danmaku.model.DanmakuTimer;
import master.flame.danmaku.danmaku.model.IDanmakus;
import master.flame.danmaku.danmaku.model.android.DanmakuContext;
import master.flame.danmaku.danmaku.model.android.Danmakus;
import master.flame.danmaku.danmaku.parser.BaseDanmakuParser;
import master.flame.danmaku.ui.widget.DanmakuView;

import static com.battlecryinc.tclips.clips.bean.Clip.ClipsEntity.ChatEntity;
import static com.battlecryinc.tclips.clips.bean.Clip.ClipsEntity.ChatEntity.DataEntity;
import static com.battlecryinc.tclips.clips.delegate.ClipViewDelegate.dp2px;
import static com.battlecryinc.tclips.clips.delegate.ClipViewDelegate.getVodTimeInS;
import static com.battlecryinc.tclips.clips.delegate.ClipViewDelegate.sp2px;

/**
 * Created by geoff on 17/4/12.
 */

public class FullScreenVideoActivity extends BaseActivity
        implements FullScreenVideoContract.IFullScreenVideoView {

    @BindView(R2.id.clips_video_player)
    BetterVideoPlayer mBetterVideoPlayer;
    @BindView(R2.id.fullScreenDanmaku)
    DanmakuView mDanmakuView;

    @Inject
    FullScreenVideoPresenter fullScreenVideoPresenter;

    private DanmakuContext mDanmakuContext;
    private BaseDanmakuParser mDanmakuParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.clips_activity_fullscreen_video);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ButterKnife.bind(this);

        mDanmakuContext = DanmakuContext.create();
        mDanmakuParser = new BaseDanmakuParser() {
            @Override
            protected IDanmakus parse() {
                return new Danmakus();
            }
        };

        DaggerFullScreenVideoComponent.builder()
                .fullScreenVideoModule(new FullScreenVideoModule(this))
                .build()
                .inject(this);

        fullScreenVideoPresenter.start();
    }

    @Override
    public void initPlayer(String title, int progress, Uri uri, Bundle bundle){
        final ChatEntity entity = (ChatEntity) bundle.getSerializable("chat_e");
        final String vodUrl = bundle.getString("vod_url");
        mBetterVideoPlayer.setTitle(title);
//        mBetterVideoPlayer.seekTo(progress);
        mBetterVideoPlayer.setAutoPlay(NetWorkUtil.isWifiConnected(this));
        mBetterVideoPlayer.setCallback(new BetterVideoCallback() {

            boolean isNeedRestartDM = false;

            @Override public void onPreparing(BetterVideoPlayer player) {}
            @Override public void onBuffering(int percent) {}
            @Override public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {}
            @Override public void onPrepared(BetterVideoPlayer player) {}
            @Override public void onError(BetterVideoPlayer player, Exception e) {
                player.reset();
                if (mDanmakuView != null) {
                    isNeedRestartDM = true;
                }
            }
            @Override public void onStarted(BetterVideoPlayer player) {
                if (mDanmakuView != null && mDanmakuView.isPrepared() && mDanmakuView.isPaused()) {
                    mDanmakuView.resume();
                }else if (mDanmakuView != null && isNeedRestartDM && !mDanmakuView.isPrepared()){
                    isNeedRestartDM = false;
                    mDanmakuView.prepare(mDanmakuParser, mDanmakuContext);
                }else {
                    initDanmaku(entity, vodUrl);
                }
            }
            @Override public void onPaused(BetterVideoPlayer player) {
                if (mDanmakuView != null && mDanmakuView.isPrepared()) {
                    mDanmakuView.pause();
                }
            }
            @Override public void onCompletion(BetterVideoPlayer player) {
                if (mDanmakuView != null && mDanmakuView.isPrepared()) {
                    isNeedRestartDM = true;
                    mDanmakuView.stop();
                }
            }
        });
        mBetterVideoPlayer.setMenu(R.menu.clips_video_fullscreen);
        mBetterVideoPlayer.setMenuCallback(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_danma){
                    if (mDanmakuView.isShown()) {
                        mDanmakuView.hide();
                    }else {
                        mDanmakuView.show();
                    }
                }else if (id == R.id.action_fullscreen){
                    finish();
                }
                return false;
            }
        });
        mBetterVideoPlayer.setSource(uri);

        mDanmakuView.setCallback(new DrawHandler.Callback() {

            @Override public void updateTimer(DanmakuTimer timer) {}
            @Override public void danmakuShown(BaseDanmaku danmaku) {}
            @Override public void drawingFinished() {}
            @Override public void prepared() {
                mDanmakuView.start();
                initDanmaku(entity, vodUrl);
            }
        });
        mDanmakuView.prepare(mDanmakuParser, mDanmakuContext);
    }

    private void initDanmaku(ChatEntity entity, String vodUrl){
        if (!mDanmakuView.isPrepared() || !mBetterVideoPlayer.isPrepared()){
            return;
        }
        final int padding = (int)dp2px(this, 3.0f);
        final int tSize = sp2px(this, 15.0f);
        for (DataEntity data : entity.getData()){
            BaseDanmaku danmaku = mDanmakuContext.mDanmakuFactory.createDanmaku(BaseDanmaku.TYPE_SCROLL_RL);
            danmaku.text = data.getAttributes().getMessage();
            if (!TextUtils.isEmpty(data.getAttributes().getColor())) {
                danmaku.textColor = Color.parseColor(data.getAttributes().getColor());
            }
            danmaku.setTime(mDanmakuView.getCurrentTime() + (data.getAttributes().getVideooffset() -
                    getVodTimeInS(vodUrl) * 1000));
            danmaku.padding = padding;
            danmaku.textSize = tSize;
            mDanmakuView.addDanmaku(danmaku);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBetterVideoPlayer != null && !mBetterVideoPlayer.isPrepared()){
            mBetterVideoPlayer.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBetterVideoPlayer != null){
            mBetterVideoPlayer.stop();
            mBetterVideoPlayer.release();
            mBetterVideoPlayer = null;
        }
        if (mDanmakuView != null) {
            mDanmakuView.release();
            mDanmakuView = null;
        }
    }

    @Override
    public Activity getActivity() {
        return this;
    }
}
