package com.battlecryinc.tclips.clips.bean;

/**
 * Created by geoff on 17/3/31.
 */

public class FollowChannel {

    /**
     * channel : {"_id":129454141,"broadcaster_language":null,"created_at":"2016-07-13T14:40:42Z","display_name":"dallasnchains","followers":2,"game":null,"language":"en","logo":null,"mature":null,"name":"dallasnchains","partner":false,"profile_banner":null,"profile_banner_background_color":null,"status":null,"updated_at":"2016-12-14T00:32:17Z","url":"https://www.twitch.tv/dallasnchains","video_banner":null,"views":6}
     * created_at : 2016-12-14T10:28:32-08:00
     * notifications : false
     */

    private ChannelEntity channel;
    private String created_at;
    private boolean notifications;

    public ChannelEntity getChannel() {
        return channel;
    }

    public void setChannel(ChannelEntity channel) {
        this.channel = channel;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public boolean isNotifications() {
        return notifications;
    }

    public void setNotifications(boolean notifications) {
        this.notifications = notifications;
    }

    public static class ChannelEntity {
        /**
         * _id : 129454141
         * broadcaster_language : null
         * created_at : 2016-07-13T14:40:42Z
         * display_name : dallasnchains
         * followers : 2
         * game : null
         * language : en
         * logo : null
         * mature : null
         * name : dallasnchains
         * partner : false
         * profile_banner : null
         * profile_banner_background_color : null
         * status : null
         * updated_at : 2016-12-14T00:32:17Z
         * url : https://www.twitch.tv/dallasnchains
         * video_banner : null
         * views : 6
         */

        private int _id;
        private Object broadcaster_language;
        private String created_at;
        private String display_name;
        private int followers;
        private Object game;
        private String language;
        private Object logo;
        private Object mature;
        private String name;
        private boolean partner;
        private Object profile_banner;
        private Object profile_banner_background_color;
        private Object status;
        private String updated_at;
        private String url;
        private Object video_banner;
        private int views;

        public int get_id() {
            return _id;
        }

        public void set_id(int _id) {
            this._id = _id;
        }

        public Object getBroadcaster_language() {
            return broadcaster_language;
        }

        public void setBroadcaster_language(Object broadcaster_language) {
            this.broadcaster_language = broadcaster_language;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getDisplay_name() {
            return display_name;
        }

        public void setDisplay_name(String display_name) {
            this.display_name = display_name;
        }

        public int getFollowers() {
            return followers;
        }

        public void setFollowers(int followers) {
            this.followers = followers;
        }

        public Object getGame() {
            return game;
        }

        public void setGame(Object game) {
            this.game = game;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public Object getLogo() {
            return logo;
        }

        public void setLogo(Object logo) {
            this.logo = logo;
        }

        public Object getMature() {
            return mature;
        }

        public void setMature(Object mature) {
            this.mature = mature;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isPartner() {
            return partner;
        }

        public void setPartner(boolean partner) {
            this.partner = partner;
        }

        public Object getProfile_banner() {
            return profile_banner;
        }

        public void setProfile_banner(Object profile_banner) {
            this.profile_banner = profile_banner;
        }

        public Object getProfile_banner_background_color() {
            return profile_banner_background_color;
        }

        public void setProfile_banner_background_color(Object profile_banner_background_color) {
            this.profile_banner_background_color = profile_banner_background_color;
        }

        public Object getStatus() {
            return status;
        }

        public void setStatus(Object status) {
            this.status = status;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Object getVideo_banner() {
            return video_banner;
        }

        public void setVideo_banner(Object video_banner) {
            this.video_banner = video_banner;
        }

        public int getViews() {
            return views;
        }

        public void setViews(int views) {
            this.views = views;
        }
    }
}
