package com.battlecryinc.tclips.clips.delegate;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.battlecryinc.tclips.clips.R;
import com.battlecryinc.tclips.clips.R2;
import com.battlecryinc.tclips.clips.bean.NavMenu;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.lujun.buildingblocks.BaseDelegate;

/**
 * Created by geoff on 17/3/29.
 */

public class NavMenuDelegate extends BaseDelegate<List> {

    private AdapterView.OnItemClickListener mOnItemClickListener;

    private int mCheckedColor = Color.parseColor("#6441a5");

    public NavMenuDelegate(AdapterView.OnItemClickListener onItemClickListener){
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public boolean isForViewType(@NonNull List items, int position) {
        return items.get(position) instanceof NavMenu;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new NavMenuHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.clips_item_nav, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull List items, final int position,
                                 @NonNull RecyclerView.ViewHolder holder) {
        NavMenuHolder navMenuHolder = (NavMenuHolder) holder;

        NavMenu menu = (NavMenu) items.get(position);

        navMenuHolder.tvNavTitle.setVisibility(position == 0 ||
                menu.getType() != ((NavMenu) items.get(position - 1)).getType() ? View.VISIBLE : View.GONE);
        navMenuHolder.tvNavTitle.setText(menu.getTitle());
        navMenuHolder.tvNavContent.setTextColor(menu.isChecked() ?
                mCheckedColor : Color.WHITE);
        if (menu.getType() == NavMenu.MENU_T.TWITCH_GAME) {
            navMenuHolder.ivNavIcon.setImageURI(menu.getTopEntity().getGame().getBox().getSmall());
            navMenuHolder.tvNavContent.setText(menu.getTopEntity().getGame().getName());
        }else {
            navMenuHolder.ivNavIcon.setImageResource(menu.getNavMenuEntity().getIcon());
            navMenuHolder.tvNavContent.setText(menu.getNavMenuEntity().getContent());
        }
        navMenuHolder.llNavMenu.setBackgroundResource(menu.isChecked() ?
                R.color.clips_nav_menu_pressed : R.drawable.clips_game_selector);
    }

    class NavMenuHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R2.id.iv_nav_icon)
        SimpleDraweeView ivNavIcon;
        @BindView(R2.id.tv_nav_title)
        TextView tvNavTitle;
        @BindView(R2.id.tv_nav_content)
        TextView tvNavContent;
        @BindView(R2.id.nav_view_divider)
        View divider;
        @BindView(R2.id.ll_nav_menu)
        LinearLayout llNavMenu;

        public NavMenuHolder(View v){
            super(v);
            ButterKnife.bind(this, v);
            llNavMenu.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mOnItemClickListener != null){
                mOnItemClickListener.onItemClick(null, null, getLayoutPosition(), -1);
            }
        }
    }
}
