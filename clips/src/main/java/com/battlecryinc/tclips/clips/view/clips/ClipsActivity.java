package com.battlecryinc.tclips.clips.view.clips;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.battlecryinc.tclips.clips.R;
import com.battlecryinc.tclips.clips.R2;
import com.battlecryinc.tclips.clips.component.DaggerClipsComponent;
import com.battlecryinc.tclips.clips.contract.ClipsContract;
import com.battlecryinc.tclips.clips.module.ClipsModule;
import com.battlecryinc.tclips.clips.presenter.clips.ClipsActivityPresenter;
import com.battlecryinc.tclips.router.baseview.BaseActivity;
import com.facebook.drawee.view.SimpleDraweeView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.lujun.buildingblocks.BaseAdapter;

public class ClipsActivity extends BaseActivity implements ClipsContract.IClipsView {

    @BindView(R2.id.srl_clips_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R2.id.clips_list)
    RecyclerView clipsList;
    @BindView(R2.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.nav_view)
    NavigationView navigationView;
    @BindView(R2.id.clips_list_progressbar)
    ProgressBar listProgressbar;

//    @BindView(R2.id.nav_progressbar)
    ProgressBar navProgressBar;
//    @BindView(R2.id.clips_nav_list)
    RecyclerView navList;
//    @BindView(R2.id.clips_nav_avatar)
    SimpleDraweeView navAvatar;
//    @BindView(R2.id.clips_nav_tv_name)
    TextView navName;
//    @BindView(R2.id.clips_nav_tv_bio)
    TextView navBio;
//    @BindView(R2.id.clips_nav_login)
    Button navLogin;
//    @BindView(R2.id.clips_nav_reload)
    Button navReload;
//    @BindView(R2.id.clips_nav_header_login)
    LinearLayout navHeaderInfoArea;

    @Inject
    ClipsActivityPresenter clipsActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.clips_activity_clips);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        initNavView();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (getClipsRefreshing()){
                    clipsActivityPresenter.requestData();
                }
            }
        });
        clipsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE){
                    LinearLayoutManager manager = (LinearLayoutManager) clipsList.getLayoutManager();
                    clipsActivityPresenter.onListScroll(manager.findFirstCompletelyVisibleItemPosition());
                }
            }
        });

        DaggerClipsComponent.builder().clipsModule(new ClipsModule(this)).build().inject(this);

        clipsActivityPresenter.start();
    }

    private void initNavView(){
        View navHeaderView = navigationView.getHeaderView(0);
        navProgressBar = (ProgressBar) navHeaderView.findViewById(R.id.nav_progressbar);
        navList = (RecyclerView) navHeaderView.findViewById(R.id.clips_nav_list);
        navAvatar = (SimpleDraweeView) navHeaderView.findViewById(R.id.clips_nav_avatar);
        navName = (TextView) navHeaderView.findViewById(R.id.clips_nav_tv_name);
        navBio = (TextView) navHeaderView.findViewById(R.id.clips_nav_tv_bio);
        navLogin = (Button) navHeaderView.findViewById(R.id.clips_nav_login);
        navReload = (Button) navHeaderView.findViewById(R.id.clips_nav_reload);
        navHeaderInfoArea = (LinearLayout) navHeaderView.findViewById(R.id.clips_nav_header_login);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clipsActivityPresenter.login();
            }
        });
        navReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clipsActivityPresenter.reloadTopGames();
            }
        });
    }

    @Override
    public void invalidateNavHeaderView(boolean in, String aUrl, String name, String bio){
        navHeaderInfoArea.setVisibility(in ? View.VISIBLE : View.GONE);
        navLogin.setVisibility(in ? View.GONE : View.VISIBLE);
        if (in){
            navAvatar.setImageURI(aUrl);
            navName.setText(name == null ? "" : name);
            navBio.setText(bio == null ? "" : bio);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.clips, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.clips_action_logout) {
            clipsActivityPresenter.logout();
            return true;
        }else if (id == R.id.clips_action_about){
            clipsActivityPresenter.aboutApp();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setRecyclerViewAdapter(BaseAdapter adapter){
        clipsList.setItemAnimator(null);
        clipsList.setAdapter(adapter);
    }

    @Override
    public void scrollList(int pos){
        clipsList.smoothScrollToPosition(pos);
    }

    @Override
    public void setNavAdapter(BaseAdapter adapter){
        navList.setAdapter(adapter);
    }

    @Override
    public void setClipsRefreshing(final boolean refreshing){
        if (swipeRefreshLayout == null){
            return;
        }
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(refreshing);
            }
        });
    }

    @Override
    public boolean getClipsRefreshing(){
        if (swipeRefreshLayout == null){
            return false;
        }
        return swipeRefreshLayout.isRefreshing();
    }

    @Override
    public void setNavProgressBarVisible(boolean visible, boolean reloadShow){
        navProgressBar.setVisibility(visible ? View.VISIBLE : View.GONE);
        navList.setVisibility(visible ? View.GONE : View.VISIBLE);
        navReload.setVisibility(reloadShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void closeDrawer(boolean anim){
        drawer.closeDrawer(GravityCompat.START, anim);
    }

    @Override
    public void post(Runnable runnable, long timeMills){
        if (toolbar == null){
            return;
        }
        toolbar.postDelayed(runnable, timeMills);
    }

    @Override
    public void setListProgressbarVisible(boolean visible){
        listProgressbar.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showSnackBar(int resId){
        Snackbar.make(toolbar, resId, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public Activity getActivity() {
        return this;
    }
}
