package com.battlecryinc.tclips.clips.retrofit.service;

import com.battlecryinc.tclips.clips.bean.Clip;
import com.battlecryinc.tclips.clips.bean.FollowChannel;
import com.battlecryinc.tclips.clips.bean.FollowedChannel;
import com.battlecryinc.tclips.clips.bean.TwitchGame;
import com.battlecryinc.tclips.router.BuildConfig;

import io.reactivex.Flowable;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by geoff on 17/3/24.
 */

public interface TwitchClipsService {

    @Headers({
        "Accept: application/vnd.twitchtv.v4+json",
        "Client-ID: " + BuildConfig.TWITCH_CLIENT_ID
    })
    @GET("/kraken/clips/top")
    Flowable<Clip> getTopClips(@Query("limit") long limit, @Query("game") String game,
                               @Query("channel") String channel, @Query("period") String period,
                               @Query("trending") boolean trending);

    @Headers({
            "Accept: application/vnd.twitchtv.v4+json",
            "Client-ID: " + BuildConfig.TWITCH_CLIENT_ID
    })
    @GET("/kraken/games/top")
    Flowable<TwitchGame> getTopGames(@Query("limit") int limit, @Query("offset") int offset);

    @Headers({
            "Accept: application/vnd.twitchtv.v5+json",
            "Client-ID: " + BuildConfig.TWITCH_CLIENT_ID
    })
    @GET("/kraken/users/{uid}/follows/channels")
    Flowable<FollowedChannel> getFollowedChannel(
            @Path("uid") String uid, @Query("limit") int limit, @Query("offset") int offset);

    @Headers({
            "Accept: application/vnd.twitchtv.v5+json",
            "Client-ID: " + BuildConfig.TWITCH_CLIENT_ID
    })
    @PUT("/kraken/users/{uid}/follows/channels/{cid}")
    Flowable<FollowChannel> followChannel(@Path("uid") String uid, @Path("cid") String cid);

    @Headers({
            "Accept: application/vnd.twitchtv.v5+json",
            "Client-ID: " + BuildConfig.TWITCH_CLIENT_ID
    })
    @DELETE("/kraken/users/{uid}/follows/channels/{cid}")
    Flowable<Response<Integer>> unFollowChannel(@Path("uid") String uid, @Path("cid") String cid);
}
