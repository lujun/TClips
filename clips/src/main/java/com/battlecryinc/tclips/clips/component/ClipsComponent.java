package com.battlecryinc.tclips.clips.component;

import com.battlecryinc.tclips.clips.module.ClipsModule;
import com.battlecryinc.tclips.clips.view.clips.ClipsActivity;

import dagger.Component;

/**
 * Created by geoff on 17/4/18.
 */

@Component(modules = ClipsModule.class)
public interface ClipsComponent {

    void inject(ClipsActivity activity);
}
