package com.battlecryinc.tclips.clips.bean;

import static com.battlecryinc.tclips.clips.bean.TwitchGame.TopEntity;

/**
 * Created by geoff on 17/3/29.
 */

public class NavMenu {

    public enum MENU_T {
        BROWSE,
        TWITCH_GAME
    }

    public enum B_MENU_T {
        MY_CLIP,
        CLIPS,
        FOLLOW_CLIPS
    }

    public static class NavMenuEntity {

        private int icon;
        private String content;

        public int getIcon() {
            return icon;
        }

        public void setIcon(int icon) {
            this.icon = icon;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    private MENU_T type;
    private String title;
    private TopEntity topEntity;
    private boolean checked;
    private B_MENU_T bMenuT;

    private NavMenuEntity navMenuEntity;

    public MENU_T getType() {
        return type;
    }

    public void setType(MENU_T type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public NavMenuEntity getNavMenuEntity() {
        return navMenuEntity;
    }

    public void setNavMenuEntity(NavMenuEntity navMenuEntity) {
        this.navMenuEntity = navMenuEntity;
    }

    public TopEntity getTopEntity() {
        return topEntity;
    }

    public void setTopEntity(TopEntity topEntity) {
        this.topEntity = topEntity;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public B_MENU_T getBMenuT() {
        return bMenuT;
    }

    public void setBMenuT(B_MENU_T bMenuT) {
        this.bMenuT = bMenuT;
    }
}
