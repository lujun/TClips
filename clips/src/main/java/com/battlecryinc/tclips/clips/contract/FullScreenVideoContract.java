package com.battlecryinc.tclips.clips.contract;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

import com.battlecryinc.tclips.baselibrary.mvp.BasePresenter;
import com.battlecryinc.tclips.baselibrary.mvp.BaseView;

/**
 * Created by geoff on 17/4/18.
 */

public interface FullScreenVideoContract {

    interface IFullScreenVideoView extends BaseView<IFullScreenVideoPresenter> {

        Activity getActivity();
        void initPlayer(String title, int progress, Uri uri, Bundle bundle);
    }

    interface IFullScreenVideoPresenter extends BasePresenter {

    }
}
