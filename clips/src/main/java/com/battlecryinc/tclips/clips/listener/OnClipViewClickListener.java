package com.battlecryinc.tclips.clips.listener;

/**
 * Created by geoff on 17/3/31.
 */

public interface OnClipViewClickListener {

    public enum CLIP_MENU_T{
        CLIP_VIEW_AVATAR,
        CLIP_VIEW_FOLLOW,
        CLIP_VIEW_3P,
        CLIP_VIEW_FULLSCREEN,
        CLIP_VIEW_SHARE,
        CLIP_VIEW_SCROLL_2_1
    }

    void onClipViewClick(int position, CLIP_MENU_T type);
}
