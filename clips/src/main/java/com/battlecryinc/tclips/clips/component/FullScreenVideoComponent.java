package com.battlecryinc.tclips.clips.component;

import com.battlecryinc.tclips.clips.module.FullScreenVideoModule;
import com.battlecryinc.tclips.clips.view.fullscreenvideo.FullScreenVideoActivity;

import dagger.Component;

/**
 * Created by geoff on 17/4/18.
 */

@Component(modules = FullScreenVideoModule.class)
public interface FullScreenVideoComponent {

    void inject(FullScreenVideoActivity activity);
}
