package com.battlecryinc.tclips.clips.module;

import com.battlecryinc.tclips.clips.contract.FullScreenVideoContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by geoff on 17/4/18.
 */

@Module
public class FullScreenVideoModule {

    private final FullScreenVideoContract.IFullScreenVideoView view;

    public FullScreenVideoModule(FullScreenVideoContract.IFullScreenVideoView view){
        this.view = view;
    }

    @Provides
    FullScreenVideoContract.IFullScreenVideoView provideIFullScreenVideoView(){
        return view;
    }
}
