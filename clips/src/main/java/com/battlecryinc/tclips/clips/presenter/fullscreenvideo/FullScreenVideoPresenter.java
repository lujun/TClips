package com.battlecryinc.tclips.clips.presenter.fullscreenvideo;

import android.content.Intent;
import android.net.Uri;

import com.battlecryinc.tclips.clips.contract.FullScreenVideoContract;

import javax.inject.Inject;

/**
 * Created by geoff on 17/4/12.
 */

public class FullScreenVideoPresenter implements FullScreenVideoContract.IFullScreenVideoPresenter {

    private FullScreenVideoContract.IFullScreenVideoView mIFullScreenVideoView;

    @Inject
    public FullScreenVideoPresenter(FullScreenVideoContract.IFullScreenVideoView iFullScreenVideoView){
        mIFullScreenVideoView = iFullScreenVideoView;
    }

    @Override
    public void start() {
        Intent intent = mIFullScreenVideoView.getActivity().getIntent();
        mIFullScreenVideoView.initPlayer(intent.getStringExtra("title"),
                intent.getIntExtra("progress", 0), Uri.parse(intent.getStringExtra("url")),
                intent.getExtras());
    }
}
