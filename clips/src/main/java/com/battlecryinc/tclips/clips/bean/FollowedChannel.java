package com.battlecryinc.tclips.clips.bean;

import java.util.List;

/**
 * Created by geoff on 17/3/30.
 */

public class FollowedChannel {

    /**
     * _total : 27
     * follows : [{"created_at":"2016-09-16T20:37:39Z","notifications":false,"channel":{"_id":12826,"background":null,"banner":null,"broadcaster_language":"en","created_at":"2007-05-22T10:39:54Z","delay":null,"display_name":"Twitch","followers":530641,"game":"Gaming Talk Shows","language":"en","logo":"https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-profile_image-bd6df6672afc7497-300x300.png","mature":false,"name":"twitch","partner":true,"profile_banner":"https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-profile_banner-6936c61353e4aeed-480.png","profile_banner_background_color":null,"status":"Twitch Weekly","updated_at":"2016-12-13T18:35:28Z","url":"https://www.twitch.tv/twitch","video_banner":"https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-channel_offline_image-d687d9e22677a1b6-640x360.png","views":109064987}}]
     */

    private int _total;
    private List<FollowsEntity> follows;

    public int get_total() {
        return _total;
    }

    public void set_total(int _total) {
        this._total = _total;
    }

    public List<FollowsEntity> getFollows() {
        return follows;
    }

    public void setFollows(List<FollowsEntity> follows) {
        this.follows = follows;
    }

    public static class FollowsEntity {
        /**
         * created_at : 2016-09-16T20:37:39Z
         * notifications : false
         * channel : {"_id":12826,"background":null,"banner":null,"broadcaster_language":"en","created_at":"2007-05-22T10:39:54Z","delay":null,"display_name":"Twitch","followers":530641,"game":"Gaming Talk Shows","language":"en","logo":"https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-profile_image-bd6df6672afc7497-300x300.png","mature":false,"name":"twitch","partner":true,"profile_banner":"https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-profile_banner-6936c61353e4aeed-480.png","profile_banner_background_color":null,"status":"Twitch Weekly","updated_at":"2016-12-13T18:35:28Z","url":"https://www.twitch.tv/twitch","video_banner":"https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-channel_offline_image-d687d9e22677a1b6-640x360.png","views":109064987}
         */

        private String created_at;
        private boolean notifications;
        private ChannelEntity channel;

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public boolean isNotifications() {
            return notifications;
        }

        public void setNotifications(boolean notifications) {
            this.notifications = notifications;
        }

        public ChannelEntity getChannel() {
            return channel;
        }

        public void setChannel(ChannelEntity channel) {
            this.channel = channel;
        }

        public static class ChannelEntity {
            /**
             * _id : 12826
             * background : null
             * banner : null
             * broadcaster_language : en
             * created_at : 2007-05-22T10:39:54Z
             * delay : null
             * display_name : Twitch
             * followers : 530641
             * game : Gaming Talk Shows
             * language : en
             * logo : https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-profile_image-bd6df6672afc7497-300x300.png
             * mature : false
             * name : twitch
             * partner : true
             * profile_banner : https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-profile_banner-6936c61353e4aeed-480.png
             * profile_banner_background_color : null
             * status : Twitch Weekly
             * updated_at : 2016-12-13T18:35:28Z
             * url : https://www.twitch.tv/twitch
             * video_banner : https://static-cdn.jtvnw.net/jtv_user_pictures/twitch-channel_offline_image-d687d9e22677a1b6-640x360.png
             * views : 109064987
             */

            private int _id;
            private Object background;
            private Object banner;
            private String broadcaster_language;
            private String created_at;
            private Object delay;
            private String display_name;
            private int followers;
            private String game;
            private String language;
            private String logo;
            private boolean mature;
            private String name;
            private boolean partner;
            private String profile_banner;
            private Object profile_banner_background_color;
            private String status;
            private String updated_at;
            private String url;
            private String video_banner;
            private int views;

            public int get_id() {
                return _id;
            }

            public void set_id(int _id) {
                this._id = _id;
            }

            public Object getBackground() {
                return background;
            }

            public void setBackground(Object background) {
                this.background = background;
            }

            public Object getBanner() {
                return banner;
            }

            public void setBanner(Object banner) {
                this.banner = banner;
            }

            public String getBroadcaster_language() {
                return broadcaster_language;
            }

            public void setBroadcaster_language(String broadcaster_language) {
                this.broadcaster_language = broadcaster_language;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public Object getDelay() {
                return delay;
            }

            public void setDelay(Object delay) {
                this.delay = delay;
            }

            public String getDisplay_name() {
                return display_name;
            }

            public void setDisplay_name(String display_name) {
                this.display_name = display_name;
            }

            public int getFollowers() {
                return followers;
            }

            public void setFollowers(int followers) {
                this.followers = followers;
            }

            public String getGame() {
                return game;
            }

            public void setGame(String game) {
                this.game = game;
            }

            public String getLanguage() {
                return language;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public boolean isMature() {
                return mature;
            }

            public void setMature(boolean mature) {
                this.mature = mature;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public boolean isPartner() {
                return partner;
            }

            public void setPartner(boolean partner) {
                this.partner = partner;
            }

            public String getProfile_banner() {
                return profile_banner;
            }

            public void setProfile_banner(String profile_banner) {
                this.profile_banner = profile_banner;
            }

            public Object getProfile_banner_background_color() {
                return profile_banner_background_color;
            }

            public void setProfile_banner_background_color(Object profile_banner_background_color) {
                this.profile_banner_background_color = profile_banner_background_color;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getVideo_banner() {
                return video_banner;
            }

            public void setVideo_banner(String video_banner) {
                this.video_banner = video_banner;
            }

            public int getViews() {
                return views;
            }

            public void setViews(int views) {
                this.views = views;
            }
        }
    }
}
