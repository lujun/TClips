package com.battlecryinc.tclips.clips.delegate;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.battlecryinc.tclips.baselibrary.util.NetWorkUtil;
import com.battlecryinc.tclips.baselibrary.util.ScreenUtil;
import com.battlecryinc.tclips.clips.R;
import com.battlecryinc.tclips.clips.R2;
import com.battlecryinc.tclips.clips.listener.OnClipViewClickListener;
import com.battlecryinc.tclips.router.BuildConfig;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;

import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.lujun.buildingblocks.BaseDelegate;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import master.flame.danmaku.controller.DrawHandler;
import master.flame.danmaku.danmaku.model.BaseDanmaku;
import master.flame.danmaku.danmaku.model.DanmakuTimer;
import master.flame.danmaku.danmaku.model.IDanmakus;
import master.flame.danmaku.danmaku.model.android.DanmakuContext;
import master.flame.danmaku.danmaku.model.android.Danmakus;
import master.flame.danmaku.danmaku.parser.BaseDanmakuParser;
import master.flame.danmaku.ui.widget.DanmakuView;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static android.widget.AdapterView.OnItemClickListener;
import static com.battlecryinc.tclips.clips.bean.Clip.ClipsEntity;
import static com.battlecryinc.tclips.clips.bean.Clip.ClipsEntity.ChatEntity;
import static com.battlecryinc.tclips.clips.bean.Clip.ClipsEntity.ChatEntity.DataEntity;
import static com.battlecryinc.tclips.clips.bean.Clip.ClipsEntity.VideoEntity;
import static com.battlecryinc.tclips.clips.listener.OnClipViewClickListener.CLIP_MENU_T;

/**
 * Created by geoff on 17/3/27.
 */

public class ClipViewDelegate extends BaseDelegate<List> {

    private OnClipViewClickListener mClipListener;
    private OnItemClickListener mOnVideoLoadListener;
    private TwitchClipsVideoService mTClipsVideoService;
    private TwitchChatService mTwitchChatService;
    private Context mContext;
    private DanmakuContext mDanmakuContext;

    private int mCurrentFocus = -1;

    private static final String CLIPS_VIDEO_BASE = "https://clips.twitch.tv";
    private static final String CLIPS_CHAT_BASE = "https://rechat.twitch.tv";
    private static final String PARSE_KEY = "quality_options";

    private static final String TAG = "ClipViewDelegate";

    public ClipViewDelegate(OnClipViewClickListener listener, OnItemClickListener listener2){
        mClipListener = listener;
        mOnVideoLoadListener = listener2;

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY :
                HttpLoggingInterceptor.Level.NONE);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .build();

        mTClipsVideoService = new Retrofit.Builder()
                .baseUrl(CLIPS_VIDEO_BASE)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
                .create(TwitchClipsVideoService.class);

        mTwitchChatService = new Retrofit.Builder()
                .baseUrl(CLIPS_CHAT_BASE)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(TwitchChatService.class);

        mDanmakuContext = DanmakuContext.create();
    }

    @Override
    public boolean isForViewType(@NonNull List items, int position) {
        return items.get(position) instanceof ClipsEntity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        mContext = parent.getContext();
        return new ClipView2Holder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.clips_item_clip_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull List items, final int position,
                                 @NonNull RecyclerView.ViewHolder holder) {
        final ClipView2Holder clipView2Holder = (ClipView2Holder) holder;

        final ClipsEntity entity = (ClipsEntity) items.get(position);
        setVideoDataAsync(position, entity);

        clipView2Holder.ivStreamerAvatar.setImageURI(entity.getBroadcaster().getLogo());
        clipView2Holder.tvStreamerName.setText(entity.getBroadcaster().getDisplay_name());
        clipView2Holder.tvStreamerGame.setText(entity.getGame());
        clipView2Holder.ivFollow.setImageResource(entity.isFollowed() ?
                R.drawable.clips_ic_following : R.drawable.clips_ic_follow);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                clipView2Holder.rlPlayerContainer.getLayoutParams();
        params.height = ScreenUtil.get().getScreenWidth() * 9 / 16;
        clipView2Holder.rlPlayerContainer.setLayoutParams(params);
        clipView2Holder.tvTitle.setText(entity.getTitle());
        clipView2Holder.tvClipper.setText(formatClipTime(mContext, entity.getCreated_at()) + " by "
                + entity.getCurator().getDisplay_name());
        clipView2Holder.tvViewTimes.setText(NumberFormat.getNumberInstance().format(entity.getViews()));
        clipView2Holder.ivClipThumbNail.setImageURI(entity.getThumbnails().getMedium());

        if (isVideoUrlLoaded(entity) && mCurrentFocus == position){
            setClipControlViewVisible(clipView2Holder, false, true, true, false);
            onVideoPlay(clipView2Holder, entity, position);
        }else if (!isVideoUrlLoaded(entity) && mCurrentFocus == position){
            setClipControlViewVisible(clipView2Holder, false, true, true, false);
        }else {
            setClipControlViewVisible(clipView2Holder, false, false, true, true);
        }
    }

    private void setClipControlViewVisible(ClipView2Holder holder, boolean videoArea,
                                           boolean loading, boolean thumb, boolean play){
        View rl0V = null;
        View rl1V = null;
        if(holder.rlPlayerContainer.getChildCount() > 0) {
            rl0V = holder.rlPlayerContainer.getChildAt(0);
        }
        if(holder.rlPlayerContainer.getChildCount() > 1) {
            rl1V = holder.rlPlayerContainer.getChildAt(1);
        }
        if (!videoArea && rl0V != null && rl0V instanceof BetterVideoPlayer) {
            BetterVideoPlayer vView = (BetterVideoPlayer) rl0V;
            holder.rlPlayerContainer.removeView(vView);
            vView.stop();
            vView.release();
            vView = null;
        }
        if (!videoArea && rl1V != null && rl1V instanceof DanmakuView) {
            DanmakuView dView = (DanmakuView) rl1V;
            holder.rlPlayerContainer.removeView(dView);
            dView.release();
            dView = null;
        }

        holder.clipsWVLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        holder.ivClipThumbNail.setVisibility(thumb ? View.VISIBLE : View.GONE);
        holder.ivPlay.setVisibility(play ? View.VISIBLE : View.GONE);
    }

    private void onVideoPlay(ClipView2Holder holder, final ClipsEntity entity, final int pos){
        setClipControlViewVisible(holder, true, false, false, false);

        LayoutInflater inflater = LayoutInflater.from(mContext);

        final DanmakuView danmakuView = (DanmakuView) inflater
                .inflate(R.layout.clips_danmaku, holder.rlPlayerContainer, false)
                .findViewById(R.id.clipDanmaku);
        final BetterVideoPlayer playerView = (BetterVideoPlayer)inflater
                .inflate(R.layout.clips_player, holder.rlPlayerContainer, false)
                .findViewById(R.id.clipVideo);

        danmakuView.setCallback(new DrawHandler.Callback() {

            @Override public void updateTimer(DanmakuTimer timer) {}
            @Override public void danmakuShown(BaseDanmaku danmaku) {}
            @Override public void drawingFinished() {}
            @Override public void prepared() {
                danmakuView.start();
                initDanmaku(danmakuView, entity, playerView);
            }
        });

        playerView.setAutoPlay(NetWorkUtil.isWifiConnected(mContext));
        playerView.setCallback(new BetterVideoCallback() {

            boolean isNeedRestartDM = false;

            @Override public void onPreparing(BetterVideoPlayer player) {}
            @Override public void onBuffering(int percent) {}
            @Override public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {}
            @Override public void onPrepared(BetterVideoPlayer player) {}
            @Override public void onError(BetterVideoPlayer player, Exception e) {
                player.reset();
                if (danmakuView != null) {
                    isNeedRestartDM = true;
                }
            }
            @Override public void onStarted(BetterVideoPlayer player) {
                if (danmakuView != null && danmakuView.isPrepared()&& danmakuView.isPaused()) {
                    danmakuView.resume();
                }else if (danmakuView != null && isNeedRestartDM && !danmakuView.isPrepared()){
                    isNeedRestartDM = false;
                    danmakuView.prepare(new BaseDanmakuParser() {
                        @Override
                        protected IDanmakus parse() {
                            return new Danmakus();
                        }
                    }, mDanmakuContext);
                }else {
                    initDanmaku(danmakuView, entity, playerView);
                }
            }
            @Override public void onPaused(BetterVideoPlayer player) {
                if (danmakuView != null && danmakuView.isPrepared()) {
                    danmakuView.pause();
                }
            }
            @Override public void onCompletion(BetterVideoPlayer player) {
                if (danmakuView != null && danmakuView.isPrepared()) {
                    isNeedRestartDM = true;
                    danmakuView.stop();
                }
            }
        });
        playerView.setMenu(R.menu.clips_video);
        playerView.setMenuCallback(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_fullscreen && mClipListener != null){
                    playerView.pause();
                    entity.setPlayProgress(playerView.getCurrentPosition());
                    mClipListener.onClipViewClick(pos, CLIP_MENU_T.CLIP_VIEW_FULLSCREEN);
                }else if (id == R.id.action_share && mClipListener != null){
                    mClipListener.onClipViewClick(pos, CLIP_MENU_T.CLIP_VIEW_SHARE);
                }else if (id == R.id.action_danma){
                    if (danmakuView.isShown()) {
                        danmakuView.hide();
                    }else {
                        danmakuView.show();
                    }
                }
                return false;
            }
        });

        danmakuView.prepare(new BaseDanmakuParser() {
            @Override
            protected IDanmakus parse() {
                return new Danmakus();
            }
        }, mDanmakuContext);
        playerView.setSource(Uri.parse(entity.getQuality_options()
                .get(entity.getQuality_options().size() - 1).getSource()));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

        holder.rlPlayerContainer.addView(playerView, 0, params);
        holder.rlPlayerContainer.addView(danmakuView, 1, params);

        setDanmakuAsync(entity, danmakuView, new OnChatLoadedCallback() {
            @Override
            public void onChatLoaded(DanmakuView view, ClipsEntity entity) {
                initDanmaku(view, entity, playerView);
            }
        });
        initDanmaku(danmakuView, entity, playerView);
    }

    private void initDanmaku(final DanmakuView danmakuView, ClipsEntity entity, BetterVideoPlayer player){
        if (!isChatLoaded(entity) || danmakuView == null || !danmakuView.isPrepared() ||
                !player.isPrepared()){
            return;
        }
        final int padding = (int)dp2px(mContext, 3.0f);
        final int tSize = sp2px(mContext, 14.0f);
        for (DataEntity data : entity.getChatEntity().getData()){
            BaseDanmaku danmaku = mDanmakuContext.mDanmakuFactory.createDanmaku(BaseDanmaku.TYPE_SCROLL_RL);
            danmaku.text = data.getAttributes().getMessage();
            if (!TextUtils.isEmpty(data.getAttributes().getColor())) {
                danmaku.textColor = Color.parseColor(data.getAttributes().getColor());
            }
            danmaku.setTime(danmakuView.getCurrentTime() + (data.getAttributes().getVideooffset() -
                    getVodTimeInS(entity.getVod().getUrl()) * 1000));
            danmaku.padding = padding;
            danmaku.textSize = tSize;
            danmakuView.addDanmaku(danmaku);
        }
    }

    private boolean isVideoUrlLoaded(ClipsEntity entity){
        return entity.getQuality_options() != null && entity.getQuality_options().size() > 0;
    }

    private boolean isChatLoaded(ClipsEntity entity){
        return entity.getChatEntity() != null && entity.getChatEntity().getData() != null;
    }

    class ClipView2Holder extends RecyclerView.ViewHolder implements  View.OnTouchListener{

        @BindView(R2.id.iv_streamer_avatar)
        SimpleDraweeView ivStreamerAvatar;
        @BindView(R2.id.tv_streamer_name)
        TextView tvStreamerName;
        @BindView(R2.id.tv_streamer_game)
        TextView tvStreamerGame;
        @BindView(R2.id.iv_follow)
        ImageView ivFollow;
        @BindView(R2.id.iv_clip_menu)
        ImageView ivClipMenu;
        @BindView(R2.id.iv_clip_thumbnail)
        SimpleDraweeView ivClipThumbNail;
        @BindView(R2.id.tv_title)
        TextView tvTitle;
        @BindView(R2.id.tv_clipper)
        TextView tvClipper;
        @BindView(R2.id.tv_view_times)
        TextView tvViewTimes;
        @BindView(R2.id.rl_video_player_container)
        RelativeLayout rlPlayerContainer;
        @BindView(R2.id.iv_clips_play)
        ImageView ivPlay;
        @BindView(R2.id.iv_clips_loading)
        ProgressBar clipsWVLoading;

        public ClipView2Holder(View v){
            super(v);
            ButterKnife.bind(this, v);
            assert mClipListener != null;
            ivStreamerAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClipListener.onClipViewClick(getLayoutPosition(), CLIP_MENU_T.CLIP_VIEW_AVATAR);
                }
            });
            ivFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClipListener.onClipViewClick(getLayoutPosition(), CLIP_MENU_T.CLIP_VIEW_FOLLOW);
                }
            });
            ivClipMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClipListener.onClipViewClick(getLayoutPosition(), CLIP_MENU_T.CLIP_VIEW_3P);
                }
            });
            ivClipThumbNail.setOnTouchListener(this);
        }

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN && mClipListener != null){
                mClipListener.onClipViewClick(getLayoutPosition(), CLIP_MENU_T.CLIP_VIEW_SCROLL_2_1);
            }
            return false;
        }
    }

    private void setVideoDataAsync(final int pos, final ClipsEntity entity){
        if (isVideoUrlLoaded(entity) || !NetWorkUtil.isNetworkConnected(mContext)){
            return;
        }
        mTClipsVideoService.getTwitchClipsVideo(entity.getId())
                .map(new Function<Response<ResponseBody>, List<VideoEntity>>() {
                    @Override
                    public List<VideoEntity> apply(Response<ResponseBody> response) throws Exception {
                        String html = response.body().source().readUtf8();
                        if (html != null && !TextUtils.isEmpty(html) && html.contains(PARSE_KEY)){
                            html = html.substring(html.indexOf(PARSE_KEY));
                            html = html.substring(html.indexOf("["), html.indexOf("]") + 1);

                            //[{"quality":"720","source":"https://.mp4"},{"quality":"480","source":"xx.mp4"}]
                            Gson gson = new Gson();
                            return gson.fromJson(html, new TypeToken<List<VideoEntity>>(){}.getType());
                        }
                        return null;
                    }
                })
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<VideoEntity>>() {
                    @Override
                    public void accept(List<VideoEntity> videoEntities) throws Exception {
                        entity.setQuality_options(videoEntities);
                        mOnVideoLoadListener.onItemClick(null, null, pos, 0);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                    }
                });
    }

    private void setDanmakuAsync(final ClipsEntity entity, final DanmakuView view,
                                 final OnChatLoadedCallback callback){
        if (entity.getVod() == null || isChatLoaded(entity) ||
                !NetWorkUtil.isNetworkConnected(mContext)){
            return;
        }

        mTwitchChatService.getTwitchClipsChat(
                String.valueOf(getVodTimeInS(entity.getVod().getUrl())), "v" + entity.getVod().getId())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ChatEntity>() {
                    @Override
                    public void accept(ChatEntity chatEntity) throws Exception {
                        entity.setChatEntity(chatEntity);
                        callback.onChatLoaded(view, entity);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                    }
                });
    }

    public static long getVodTimeInS(String url){
        Uri uri = Uri.parse(url);
        String offsetS = uri.getQueryParameter("t");

        long timeInS;
        if (offsetS.contains("h")){
            int hIndex = offsetS.indexOf("h");
            int mIndex = offsetS.indexOf("m");
            int sIndex = offsetS.indexOf("s");
            String h = offsetS.substring(0, hIndex);
            String m = offsetS.substring(hIndex + 1, mIndex);
            String s = offsetS.substring(mIndex + 1, sIndex);
            timeInS = Integer.valueOf(h) * 60 * 60 + Integer.valueOf(m) * 60 + Integer.valueOf(s);
        }else if (offsetS.contains("m")){
            int mIndex = offsetS.indexOf("m");
            int sIndex = offsetS.indexOf("s");
            String m = offsetS.substring(0, mIndex);
            String s = offsetS.substring(mIndex + 1, sIndex);
            timeInS = Integer.valueOf(m) * 60 + Integer.valueOf(s);
        }else {
            int sIndex = offsetS.indexOf("s");
            String s = offsetS.substring(0, sIndex);
            timeInS = Integer.valueOf(s);
        }

        return timeInS;
    }

    private interface TwitchClipsVideoService{

        @GET("/embed")
        Flowable<Response<ResponseBody>> getTwitchClipsVideo(@Query("clip") String clip);
    }

    private interface TwitchChatService{

        @GET("/rechat-messages")
        Flowable<ChatEntity> getTwitchClipsChat(
                @Query("offset_seconds") String offset_seconds, @Query("video_id") String video_id);
    }

    private interface OnChatLoadedCallback{

        void onChatLoaded(DanmakuView view, ClipsEntity entity);
    }

    private String formatClipTime(Context context, String time){
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            Date tDate = df.parse(time);

            Timestamp currentStamp = new Timestamp(System.currentTimeMillis());
            Timestamp createTime = new Timestamp(tDate.getTime());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

            long timeSpend = currentStamp.getTime() - createTime.getTime();
            float days = (timeSpend) / (1000l * 60l * 60l * 24l);
            float hours = (timeSpend) / (1000l * 60l * 60l);
            float minutes = (timeSpend) / (1000l * 60l);

            String createTimeString = format.format(new Date(createTime.getTime()));
            String currentTimeString = format.format(new Date(System.currentTimeMillis()));

            int currentYear = Integer.parseInt(currentTimeString.substring(
                    0, createTimeString.indexOf("-")));
            int createYear = Integer.parseInt(createTimeString.substring(
                    0, createTimeString.indexOf("-")));
            int createDay = Integer.parseInt(createTimeString.substring(
                    createTimeString.lastIndexOf("-") + 1, createTimeString.length()));

            if (currentYear > createYear) {
                return String.valueOf(createYear);
            } else {
                if (days >= 30) {
                    return format.format(new Date(createTime.getTime()));
                } else if (days >= 1){
                    return (int) days + " " + context.getString(R.string.clips_time_trans_ds);
                } else {
                    if (hours >= 1) {
                        return (int) hours + " " + context.getString(R.string.clips_time_trans_hours);
                    } else {
                        if (minutes < 1) {
                            return 1 + " " + context.getString(R.string.clips_time_trans_minutes);
                        } else {
                            return (int) minutes + " " + context.getString(R.string.clips_time_trans_minutes);
                        }
                    }
                }
            }
        }catch (ParseException e){
            return time;
        }
    }

    public static int sp2px(Context context, float spValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

    public static float dp2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }

    public void setCurrentFocus(int pos) {
        mCurrentFocus = pos;
    }

    public int getCurrentFocus() {
        return mCurrentFocus;
    }
}
