package com.battlecryinc.tclips.clips.module;

import com.battlecryinc.tclips.clips.contract.ClipsContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by geoff on 17/4/18.
 */

@Module
public class ClipsModule {

    private final ClipsContract.IClipsView view;

    public ClipsModule(ClipsContract.IClipsView view){
        this.view = view;
    }

    @Provides
    ClipsContract.IClipsView provideIClipsView(){
        return view;
    }
}
