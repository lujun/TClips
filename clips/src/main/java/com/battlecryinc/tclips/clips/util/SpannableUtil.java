package com.battlecryinc.tclips.clips.util;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

/**
 * Created by geoff on 17/3/29.
 */

public class SpannableUtil {

    private static final int COMMENT_FROM_COLOR = Color.BLACK;
    private static final int COMMENT_TO_COLOR = Color.BLACK;
    private static final int COMMENT_REPLY_COLOR = Color.parseColor("#6441a5");

    public static SpannableStringBuilder generateCommentLine(@NonNull String from, String to, @NonNull String comment){
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString sFrom, sReply, sTo, sComment;

        from = to == null ? from + ":" : from;
        sFrom = new SpannableString(from);
        ForegroundColorSpan fromColorSpan = new ForegroundColorSpan(COMMENT_FROM_COLOR);
        AbsoluteSizeSpan fromSizeSpan = new AbsoluteSizeSpan(14, true);
        StyleSpan fromStyleSpan = new StyleSpan(Typeface.BOLD);
        sFrom.setSpan(fromColorSpan, 0, from.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sFrom.setSpan(fromSizeSpan, 0, from.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sFrom.setSpan(fromStyleSpan, 0, from.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        sComment = new SpannableString(comment);
        ForegroundColorSpan commentColorSpan = new ForegroundColorSpan(COMMENT_TO_COLOR);
        AbsoluteSizeSpan commentSizeSpan = new AbsoluteSizeSpan(13, true);
        sComment.setSpan(commentColorSpan, 0, comment.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sComment.setSpan(commentSizeSpan, 0, comment.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        if (to != null) {
            String reply = "reply";
            sReply = new SpannableString(reply);
            ForegroundColorSpan replyColorSpan = new ForegroundColorSpan(COMMENT_REPLY_COLOR);
            AbsoluteSizeSpan replySizeSpan = new AbsoluteSizeSpan(14, true);
            sReply.setSpan(replyColorSpan, 0, reply.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            sReply.setSpan(replySizeSpan, 0, reply.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            to = to + ":";
            sTo = new SpannableString(to);
            ForegroundColorSpan toColorSpan = new ForegroundColorSpan(COMMENT_TO_COLOR);
            AbsoluteSizeSpan toSizeSpan = new AbsoluteSizeSpan(14, true);
            StyleSpan toStyleSpan = new StyleSpan(Typeface.BOLD);
            sTo.setSpan(toColorSpan, 0, to.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            sTo.setSpan(toSizeSpan, 0, to.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            sTo.setSpan(toStyleSpan, 0, to.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            builder.append(sFrom).append(" ").append(sReply).append(" ").append(sTo).append(sComment);
        }else {
            builder.append(sFrom).append(sComment);
        }

        return builder;
    }
}
