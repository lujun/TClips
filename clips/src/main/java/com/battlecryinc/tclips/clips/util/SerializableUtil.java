package com.battlecryinc.tclips.clips.util;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Author: lujun(http://blog.lujun.co)
 * Date: 30/03/2017 22:39
 */

public class SerializableUtil {

    /**
     * write a Serializable Object into cache file
     */
    public static boolean saveObject(Context context, Serializable ser, String file){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = context.openFileOutput(file, Context.MODE_PRIVATE);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(ser);
            oos.flush();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }finally {
            try {
                oos.close();
            }catch (Exception e){
                e.printStackTrace();
            }
            try {
                fos.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * read the cache file as a Serializable Object
     */
    public static Serializable readObject(Context context, String file){
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = context.openFileInput(file);
            ois = new ObjectInputStream(fis);
            return (Serializable) ois.readObject();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                ois.close();
            }catch (Exception e){
                e.printStackTrace();
            }
            try {
                fis.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return null;
    }

    public static boolean saveFChannelObject(Context context, String c_names, String uid){
        return saveObject(context, c_names, "_followed_channel_" + uid);
    }

    public static Serializable readFChannelObject(Context context, String uid){
        return readObject(context, "_followed_channel_" + uid);
    }
}
